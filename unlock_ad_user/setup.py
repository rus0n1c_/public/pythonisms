from setuptools import setup, find_packages

setup(
    name="unlock_user",
    version="1.0",
    packages=find_packages(),
    include_package_data=True,

    install_requires=['ldap3>=2.5',
                      'python-connectors'],

    scripts=['unlock'],

    data_files=[
    ],

    license='MIT',
    author="Alexey Gagarin",
    author_email="agagarin@incodewetrust.net",
    description="Quickly unlock Active Directory user",
    project_urls={
        "Source Code": "https://bitbucket.org/rus0n1c/pythonisms/unlock_ad_user"
    }
)
