Please, install https://git.ringcentral.com/sysops/python-connectors before installing this script!

Also, Python 3.6 and higher must be used!

the script expects AD_USERNAME, AD_PASSWORD and AD_SERVER environmental variables to be set.

Example: export AD_USERNAME="vasia.pupkin@ringcentral.com"; export AD_SERVER="sjc01-c01-adc01.ringcentral.com"; export AD_PASSWORD="somepass"; unlock -u john.dow
