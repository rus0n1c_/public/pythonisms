#!/usr/bin/python
# -*- coding: utf-8 -*-

import pymssql

class mssqlConnector():
	def __init__(self, dbserver,dbname,dbuser,dbpassword):
		
		self.dbserver = dbserver
		self.dbname = dbname
		self.dbuser = dbuser
		self.dbpassword = dbpassword
		
	def initConnection(self):
		
		try:
			self.conn = pymssql.connect(self.dbserver,self.dbuser,self.dbpassword,self.dbname,autocommit=True)
		except pymssql.OperationalError as e:
			raise RuntimeError(e)
	
	def runQuery(self,query):
		
		self.query = query
		try:
			if self.conn:
				self.cursor = self.conn.cursor()
				self.cursor.execute(self.query)
				self.conn.commit()
		except (pymssql.OperationalError, pymssql.ProgrammingError) as e:
			raise RuntimeError('Invalid SQL Query: %s' % (e))
	
	def closeConn(self):
		if self.conn:
			self.conn.close()