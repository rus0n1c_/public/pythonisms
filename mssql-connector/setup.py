from setuptools import setup

setup(name='mssql-connector', version='1.0',description='MSSQL Connector',url='https://bitbucket.org/rus0n1c/mssql-connector',author='Alexey U.Gagarin',author_email='bitbucket@incodewetrust.net',license='MIT',packages=['mssqlConnector'],install_requires=['pymssql'],zip_safe=False)
