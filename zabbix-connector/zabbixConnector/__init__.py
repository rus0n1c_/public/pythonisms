# -*- coding: utf-8 -*-
from __future__ import absolute_import  # -- To nicely arrange the modules that follow further down -- #

import pyzabbix
import re
from pyzabbix import (ZabbixAPI,
                      ZabbixAPIException)

import time
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning


class zabbixConnector():

    def __init__(self, login, password, URL, timeout=30):
        self.login = login
        self.password = password
        self.URL = URL
        self.timeout = timeout

    def initSession(self):
        # -- Initializes session with Zabbix API -- #
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        self.zapi = ZabbixAPI(self.URL)
        self.zapi.session.verify = False
        self.zapi.timeout = self.timeout

        try:
            self.zapi.login(self.login,
                            self.password)
        except ZabbixAPIException as e:
            raise RuntimeError(e)

    def zabbixGetObjectID(self, objectType, objectName):
        """
        Функция получения ID объекта в Zabbix.
        objectType - стрококая переменная, задающая тип объекта в Zabbix. Может принимать значения 'host','group','template'
        objectName - непосредственно имя объекта.

        Retrieves Object ID of Zabbix host.
        objectType - String variable. Sets the type of the Zabbix host ('host','group' or 'template')
        objectName - String variable. Sets the name of the Zabbix host.
        """
        acceptedTypes = ['host', 'group', 'template']

        if not objectType:
            raise RuntimeError('ObjectType is empty, nothing to return.')
        elif objectType not in acceptedTypes:
            raise RuntimeError('ObjectType value not accepted. Accepted values are \"host\",\"group\" or \"template\"')

        try:
            if objectType == 'host':

                # -- Multiple checks, 'cause host may have Visible name or Display name or both defined; case is also a consideration. -- #
                # .: Несколько проверок, т.к. узел сети может быть заведён в Zabbix только по имени узла, или по видимому имени, или в разных регистрах. :.

                idhost = self.zapi.host.get(filter={'host': objectName})
                idname = self.zapi.host.get(filter={'name': objectName})
                if len(idhost) > 0 and len(idname) < 1:
                    return idhost[0]['hostid']
                elif len(idname) > 0 and len(idhost) < 1:
                    return idname[0]['hostid']
                elif len(idhost) == len(idname) and len(idhost) > 0 and len(idname) > 0:
                    return idname[0]['hostid']
                elif len(idhost) < 1 and len(idname) < 1:
                    idsearch = self.zapi.host.get(search={'name': objectName})
                    return idsearch[0]['hostid']
            elif objectType == 'group':
                id = self.zapi.hostgroup.get(filter={'name': objectName})
                return id[0]['groupid']
            elif objectType == 'template':
                id = self.zapi.template.get(filter={'host': objectName})
                return id[0]['templateid']
        except IndexError as e:
            raise RuntimeError('Object: %s not found' % objectName)

    def zabbixUpdateHost(self, host, fieldsToUpdate={}):
        """
        Функция обновления атрибутов Zabbix-узла.
        Переменная host - видимое имя узла сети;
        Словарь fieldsToUpdate - поля, которые нужно обновить. Список доступных полей можно посмотреть в результате работы вызова API host.get.

        Updates attributes of Zabbix host.
        host - host's Visible name variable
        fieldsToUpdate - Dictionary. Attributes to be updated. Available host attributes may be retrieved from 'host.get' method of Zabbix API.
        """
        if not fieldsToUpdate:
            raise RuntimeError('No fields to update from user input. Exiting.')

        hostid = self.zabbixGetObjectID('host', host)

        try:
            paramsDict = {'hostid': hostid}
            paramsDict.update(fieldsToUpdate)
        except TypeError as e:
            raise RuntimeError('Host was not found in Zabbix by its visible name')

        if self.zapi and len(self.zapi.auth) > 0:
            self.zapi.host.update(paramsDict)

    def zabbixUpdateInterface(self, host, fieldsToUpdate={}):
        """
        Функция обновления атрибутов интерфейса Zabbix-узла. В Zabbix параметры интерфейса изменяются отдельно.
        Переменная host - видимое имя узла сети;
        Словарь fieldsToUpdate - поля, которые нужно обновить. Список доступных полей можно посмотреть в результате работы вызова API host.get.

        Updates attributes of host interface.
        host - host's Visible name variable
        fieldsToUpdate - Dictionary. Interface attributes to be updated. Available host interface attributes may be retrieved from 'hostinterface.get' method of Zabbix API.
        """
        if not fieldsToUpdate:
            raise RuntimeError('No fields to update from user input. Exiting.')

        hostid = self.zabbixGetObjectID('host', host)
        interfaceid = self.zapi.hostinterface.get(hostids=hostid)[0]['interfaceid']

        try:
            paramsDict = {'interfaceid': interfaceid}
            paramsDict.update(fieldsToUpdate)
        except TypeError as e:
            raise RuntimeError('Host was not found in Zabbix by its visible name')

        if self.zapi and len(self.zapi.auth) > 0:
            self.zapi.hostinterface.update(paramsDict)
        else:
            raise RuntimeError('Not authenticated to Zabbix.')

    def zabbixDeleteHost(self, host):
        """
        Функция для удаления узлов сети из Zabbix.
        Переменная host - видимое имя узла.

        Deletes host from Zabbix.
        host - host Visible name.
        """

        try:
            id = self.zabbixGetObjectID('host', host)
            self.zapi.host.delete(id)
        except (ZabbixAPIException, TypeError) as e:
            raise RuntimeError(e)

    def zabbixGetHostGroups(self, host):
        """
        Функция получения всех групп, в которых состоит узел.

        Retrieves all groups to which host belongs.
        """
        try:
            id = self.zabbixGetObjectID('host', host)
            groups = { group['groupid']: group['name'] for group in self.zapi.hostgroup.get(hostids=id) }
            return groups
        except IndexError as e:
            raise RuntimeError('Host not found. Try different name.')

    def zabbixGetHostTemplates(self, host):
        """
        Функция получения всех шаблонов, которые назначены на узел

        Retrieves all templates assigned to host.
        """
        try:
            id = self.zabbixGetObjectID('host', host)
            templates = { template['templateid']: template['name'] for template in self.zapi.template.get(hostids=id) }
            return templates
        except IndexError as e:
            raise RuntimeError('Host not found. Try different name.')

    def zabbixGetHostTriggers(self, host, active=True):
        """
        Функция получения всех триггеров, активных на узле host
        active - только активные триггеры в данный момент, если в True либо вообще все триггеры, если False

        Retrieves all host triggers.
        active - flag. If set to True, than only active triggers are returned. Otherwise all triggers associated with host are returned.
        """
        try:
            id = self.zabbixGetObjectID('host', host)
            state = '1' if active else '0'
            triggers = { trigger['triggerid']: trigger['description'] for trigger in self.zapi.trigger.get(hostids=id,
                                                                                                           filter={'value': state}) }
            return triggers
        except IndexError as e:
            raise RuntimeError('Host not found. Try different name.')

    def zabbixAssignGroupsToHosts(self, hosts=[], groupsToAssign=[], replace=False):
        """
        Функция назначения групп узлу/узлам сети.
        Переменная hosts - видимое имя узла или узлов сети;
        Список groupsToAssign - список групп (в тектсовом виде), который требуется назначить узлу/узлам сети;
        Перменная replace опеределяет, надо ли убрать узел Zabbix из всех предыдущих групп.

        Assigns group to single or several hosts.
        hosts - list of Visible Name of hosts.
        groupsToAssign - list of groups to assign.
        replace - flag. If set to True, than all previous groups are replaced with the ones defined in groupsToAssign. Otherwise new groups are added along with the old ones.
        """
        if not hosts:
            raise RuntimeError('Hosts list is empty.')
        elif not groupsToAssign:
            raise RuntimeError('Groups list is empty.')

        try:
            hostids = [{'hostid': self.zabbixGetObjectID('host', hid)} for hid in hosts]
            groupids = [{'groupid': self.zabbixGetObjectID('group', gid)} for gid in groupsToAssign]
            if not replace:
                self.zapi.hostgroup.massadd(groups=groupids, hosts=hostids)
            if replace:
                self.zapi.host.massupdate(hosts=hostids, groups=groupids)
        except ZabbixAPIException as e:
            RuntimeError(e)

    def zabbixAssignTemplatesToHosts(self, hosts=[], templatesToAssign=[], replace=False):
        """
        Функция назначения шаблонов узлу/узлам сети.
        Переменная hosts - видимое имя узла сети;
        Список templatesToAssign - список шаблонов (в тектсовом виде), который требуется назначить узлу/узлам сети;

        Assigns templates to host/hosts.
        templatesToAssign - list of templates to assign.
        replace - flag. If set to True, than all previous templates are replaced with the ones defined in templatesToAssign. Otherwise new templates are added along with the old ones.
        """
        if not hosts:
            raise RuntimeError('Hosts list is empty.')
        elif not templatesToAssign:
            raise RuntimeError('Templates list is empty.')

        try:
            hostids = [{'hostid': self.zabbixGetObjectID('host', hid)} for hid in hosts]
            templateids = [{'templateid': self.zabbixGetObjectID('template', tid)} for tid in templatesToAssign]
            if not replace:
                self.zapi.template.massadd(hosts=hostids, templates=templateids)
            elif replace:
                self.zapi.host.massupdate(hosts=hostids, templates=templateids)
        except ZabbixAPIException as e:
            raise RuntimeError(e)

    def zabbixGetHost(self, host):
        """
        Функция получения данных о единичном узле сети. Переменная host - видимое имя узла сети.

        Retrieves info about single host. 'host' variable is the display name of the host.
        """
        self.hostraw = self.zapi.host.get(filter={'name': host})
        self.hostinfo = {}

        try:
            self.hostinfo['Name'] = self.hostraw[0]['name']
            self.hostinfo['HostID'] = self.hostraw[0]['hostid']
            self.hostinfo['InterfaceID'] = self.zapi.hostinterface.get(hostids=self.hostraw[0]['hostid'])[0]['interfaceid']
            self.hostinfo['Description'] = self.hostraw[0]['description']
            self.hostinfo['IP-Address'] = self.zapi.hostinterface.get(hostids=self.hostraw[0]['hostid'])[0]['ip']
            self.hostinfo['DNS-Name'] = self.zapi.hostinterface.get(hostids=self.hostraw[0]['hostid'])[0]['dns']
            self.hostinfo['Status'] = 'Monitored' if self.hostraw[0]['status'] == '0' else 'Deactivated'
            self.hostinfo['ZabbixTemplate'] = self.zabbixGetHostTemplates(host)
            self.hostinfo['ZabbixGroup'] = self.zabbixGetHostGroups(host)
            return self.hostinfo
        except IndexError as e:
            raise RuntimeError('Host not found')

    def zabbixSearchHost(self, searchFilter, searchRegex):
        """
        Функция поиска и получения данных узла/узлов сети по имени и фильтру.
        searchFilter - строка поиска узла сети. Например, 'SRV'.
        searchRegex - регулярное выражение для более точной выборки узлов сети, т.к. API Zabbix не поддерживает поиск по регулярному выражению. (т.е. обработка окончательная выборка - уже в функции).

        Searches and retrieves host/hosts data matching the name and/or filter.
        searchFilter - search string partially matching zabbix node name. F.e., 'SRV'.
        searchRegex - regular expression used to narrow down the host list. Zabbix API does not support RexExps natively, so we have to use our own RegEx sorting.
        """
        searchFilter = searchFilter if type(searchFilter) == str else str(searchFilter)
        searchRegex = re.compile(searchRegex)
        self.searchResult = []

        if len(searchFilter) < 1:
            raise RuntimeError('Search filter is empty. Aborting.')
        else:
            searchResultRaw = self.zapi.host.get(search={'name': searchFilter})
            for item in searchResultRaw:
                if re.match(searchRegex, item['name']):
                    self.searchResult.append(self.zabbixGetHost(item['name']))
            return self.searchResult

    def zabbixCreateHost(self, host, groups, templates, interfaces, description=''):
        """
        Функция создания узла сети в Zabbix
        groups - список из имён групп, в которых должен состоять узел сети
        templates - список из имён шаблонов, которые должны быть применены к узлу
        interfaces - список интерфейсов узла сети; параметры интерфейса - словарь с IP-адресом, DNS-именем и т.д, например, [{'ip':'1.1.1.1','dns':'eth0.domain.local','type':'2'},{'ip':'2.2.2.2','dns':'eth1.domain.local'}]
        description - необязательная переменная, описание узла сети.

        Creates host in Zabbix.
        groups - group list to assign to new host.
        templates - template list to assign to new host.
        interfaces - list of host network interfaces. Interface parameters is dictionary with IP-address, DNS-Name etc: [{'ip':'1.1.1.1','dns':'eth0.domain.local','type',:'2'},{'ip':'2.2.2.2','dns':'eth1.domain.local']
        """

        if not isinstance(groups, list) or not isinstance(templates, list) or not isinstance(interfaces, list):
            raise RuntimeError('One or more variables are of wrong type')

        groupids = [ {'groupid': self.zabbixGetObjectID('group', id)} for id in groups ]
        templateids = [ {'templateid': self.zabbixGetObjectID('template', id)} for id in templates ]

        try:
            self.zapi.host.create(host=host.lower(), groups=groupids, templates=templateids, interfaces=interfaces, description=description, name=host)
        except ZabbixAPIException as e:
            raise RuntimeError(e)

    def zabbixGetAllGroups(self):
        """
        Возвращает все группы, заданные в Zabbix.

        Returns all groups defined in Zabbix.
        """

        groupList = self.zapi.hostgroup.get()
        groupDict = { group['name']: group['groupid'] for group in groupList }
        return groupDict

    def zabbixGetAllTemplates(self):
        """
        Возвращает все шаблоны, которые заведены в Zabbix, в формате {'<имя шаблона>' : <'ID шаблона'>}. Использует функцию zabbixGetObjectID для получения ID.

        Returns all templates defined in Zabbix, format {'<template name>' : <'template ID'}. Uses zabbixGetObjectID function for getting template ID.
        """

        templateList = self.zapi.template.get()
        templateDict = { template['host']: self.zabbixGetObjectID('template', template['host']) for template in templateList }
        return templateDict

    def zabbixGetDeactivated(self, objectType):
        """
        Возвращает узлы сети, триггеры в состоянии 'Деактивировано'

        Returns hosts, triggers in 'Deactivated' state.
        Returns {'<trigger name>' : 'hostid'} for 'triggers' object. Returns {'<hostname>' : <'hostid'>} for 'hosts' object.
        """
        acceptedTypes = ['hosts', 'triggers']

        if not objectType:
            raise RuntimeError('ObjectType is empty, nothing to return.')
        if objectType not in acceptedTypes:
            raise RuntimeError('ObjectType value not accepted. Accepted values are "hosts","triggers" or "metrics".')

        if objectType == 'hosts':
            raw = self.zapi.host.get(filter={'status': '1'})
            deactivatedHosts = { host['name']: host['hostid'] for host in raw }
            return deactivatedHosts
        if objectType == 'triggers':
            raw = self.zapi.trigger.get(filter={'status': '1'}, selectHosts='1')
            deactivatedTriggers = { trigger['description']: trigger['hosts'] for trigger in raw }
            return deactivatedTriggers
