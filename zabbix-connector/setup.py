from setuptools import setup

setup(name='zabbix-connector',
      version='1.0',
      description='Zabbix API Connector',
      url='https://bitbucket.org/rus0n1c/zabbix-connector',
      author='rus0n1c',
      author_email='bitbucket@incodewetrust.net',
      license='MIT',
      packages=['zabbixConnector'],
      install_requires=['pyzabbix'],
      zip_safe=False)
