﻿# -*- coding: utf-8 -*-

import argparse
import re
from zabbixConnector import zabbixConnector

def parserFunction(argv=None):

	parser = argparse.ArgumentParser(description="Deletes duplicate hosts in Zabbix.")
	parser.add_argument('--url',help='Zabbix-server URL',required=True)
	parser.add_argument('--login',help='Username',required=True)
	parser.add_argument('--password',help='Password',required=True)
	parser.add_argument('--grouptocheck',help='Optional. Only hosts in that group are checked for  duplicates if supplied',required=False)
	parser.add_argument('--onlyprint',action='store_true',help='Optional. No real action done if set, just prints duplicated hosts to stdout',default=False)
	parsedParams = parser.parse_args(argv)
	return parsedParam
	
def zbxDeleteDuplicates(host,login,password,grouptocheck=None,onlyprint=None):
	
	zsession = zabbixConnector(login,password,host)
	try:
		zsession.initSession()
	except RuntimeError as e:
		print(e)
		exit(1)

	if not grouptocheck:
		allhostsraw = zsession.zapi.host.get()
		allhosts = [ node['host'] for node in allhostsraw ]
		
		seen = set()
		duplicates = []
		for double in allhosts:
			if double not in seen:
				seen.add(double)
			else:
				duplicates.append(double)
		
		if onlyprint:
			for duplicate in duplicates:
				print('Найден дубликат: %s' % duplicate)
			exit(0)
		else:
			for duplicate in duplicates:
				ids = zsession.zapi.host.get(filter={'host':duplicate})
				if int(ids[0]['hostid']) < int(ids[1]['hostid'])
					print('Deleting host %s with ID %s' % (ids[0]['name'],ids[1]['name']))
						zapi.host.delete(ids[0]['hostid'])
	
	elif grouptocheck != None or grouptocheck != 'False':
		group = zapi.hostgroup.get(filter={'name':grouptocheck})
		groupid = group[0]['groupid']

		hostsFromGroupRaw = zapi.host.get(groupids=[groupid])
		hostsFromGroup = [node['host'] for node in hostsFromGroupRaw]
		
		seen = set()
		duplicates = []
		for double in hostsFromGroup:
			if double not in seen:
				seen.add(double)
			else:
				duplicates.append(double)

		if onlyprint:
			for duplicate in duplicates:
				print('Duplicate found in group %s : %s' % (grouptocheck, duplicate))
			exit(0)
		else:
			for duplicate in duplicates:
				ids = zapi.host.get(filter={'host':duplicate})
				if int(ids[0]['hostid']) < int(ids[1]['hostid']):
					print( 'Deleting duplicate %s with ID %s from group %s' % (ids[0]['name'], ids[0]['hostid'], grouptocheck) )
					zapi.host.delete(ids[0]['hostid'])
				elif int(ids[1]['hostid']) < int(ids[0]['hostid']):
					print( 'Deleting duplicate %s with ID %s from group %s' % (ids[1]['name'], ids[1]['hostid'], grouptocheck) )
					zapi.host.delete(ids[1]['hostid'])
				
if __name__ == "__main__":

	parsedValues = parserFunction()
	urlCheck = re.compile('^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$')
	if urlCheck.match(parsedValues.url):
		if not parsedValues.grouptocheck and parsedValues.onlyprint:
			
			print("'grouptocheck' is not supplied, 'onlyprint' flag is set. Printing duplicates of all Zabbix hosts to stdout.")
			print('--------------------------------------------------------------------------------------------------------'))
			zbxDeleteDuplicates(parsedValues.url,parsedValues.login,parsedValues.password,None,parsedValues.onlyprint)
		elif not parsedValues.grouptocheck and not parsedValues.onlyprint:
			print("'grouptocheck' is not supplied, 'onlyprint' flag is not set. Deleting all duplicates of all Zabbix hosts. ")
			print('----------------------------------------------------------------------------------------------------')
			zbxDeleteDuplicates(parsedValues.url,parsedValues.login,parsedValues.password)
		elif parsedValues.grouptocheck and parsedValues.onlyprint:
			print("Found group %s, 'onlyprint' flag is set. Printing duplicates" % parsedValues.grouptocheck)
			print('-----------------------------------------------------------------------------------------------------')
			zbxDeleteDuplicates(parsedValues.url,parsedValues.login,parsedValues.password,parsedValues.grouptocheck,parsedValues.onlyprint)
		elif parsedValues.grouptocheck:
			print("Found group %s 'onlyprint' flag is not set. Deleting duplicates." % parsedValues.grouptocheck)
			print('----------------------------------------------------------------------------------------------')
			zbxDeleteDuplicates(parsedValues.url,parsedValues.login,parsedValues.password,parsedValues.grouptocheck)
	else:
		print('Check Zabbix URL!')