﻿# -*- coding: utf-8 -*-

import argparse
import re
from zabbixConnector import zabbixConnector

def parserFunction(argv=None):
		
	parser = argparse.ArgumentParser(description="Autoassigns host to Zabbix group supplied in 'groupmembership' UserParameter")
	parser.add_argument('--url',help='Zabbix URL',required=True)
	parser.add_argument('--login',help='Username',required=True)
	parser.add_argument('--password',help='Password',required=True)
	parser.add_argument('--zabbixhost',help='Single Zabbix host to be checked',required=False)	
	parser.add_argument('--grouptoassign',type=lambda s: unicode(s, 'utf8'),help="Must be supplied if 'zabbixhost' is set. Group to be assigned to single host.",required=False)
	parsedParams = parser.parse_args(argv)
	if parsedParams.grouptoassign and parsedParams.zabbixhost is None:
		parser.error("--grouptoassign requires --zabbixhost variable to be set")
	return parsedParams

def groupAssigner(host,login,password,autoreg=True,zbxhostname='',grouptoassign=None,genericgroup='Отстойник'):
	"""
	autoreg  		Если True, то обработка новых, зарегистрированных между запусками скрипта, узлов. 
					Если False - проверка на изменившийся ключ groupmembership у уже существующих узлов.
	genericgroup	Группа по-умолчанию, в которую добавляются узлы при авторегистрации. При  параметре autoreg в True ищем в узлы ней
	
	autoreg			If set to True than newly registered hosts are processed. If set to False than already present hosts are checked for changed 'groupmembership' parameter.
	genericgroup	Default group to which Zabbix assigns autoregistered hosts. If 'autoreg' is set True than searching in this group.
	"""
	
	# -- Special groups to be processed.  For now it is IT dept comps -- #

	fixedGroups = ['Support Computers Adm','Support Computers', 'Infra Computers']
	
	zsession = zabbixConnector.zabbixConnector(login,password,host)
	try:
		zsession.initSession
	except RuntimeError as e:
		print(e)
		
	if autoreg:
		
		# -- Getting Template ID for 'Template OS Windows Stick And Kiosks ans Workstations' -- #
		assignedTemplate = zsession.zabbixGetObjectID('template','Template OS Windows Stick And Kiosks ans Workstations')
		
		# -- Searching in genericgroup. Deleting any other groups assigned to host, leaving only genericgroup. -- #
		tempGroupID = zsession.zabbixGetObjectID('group',genericgroup)
		
		hostsOfTempGroup = 	[ zhost['hostid'] for zhost in zsession.zapi.host.get(groupids=tempGroupID) ]
		for zhost in hostsOfTempGroup:
			hostGroups = zsession.zabbixGetHostGroups(zhost)
			hostGroups = hostGroups.keys()
			if tempGroupID in hostGroups:
				hostGroups.remove(tempGroupID)
			zsession.zapi.hostgroup.massremove(groupids=hostGroups,hostids=zhost)
			
		if tempGroupID:
			for i in hostsOfTempGroup:
				if len(zsession.zapi.item.get(hostids=i, search={'key_':'groupmembership'})) > 0:
					if not grouptoassign:
						if str(zsession.zapi.item.get(hostids=i, search={'key_':'groupmembership'})[0]['lastvalue']) in fixedGroups:
							groupToAssign = u'Workstations/IT Infra computers'
						else:
							groupToAssign = u'Workstations/Dept %s' % (zsession.zapi.item.get(hostids=i, search={'key_':'groupmembership'})[0]['lastvalue'])
					try:
						# -- Toggling  DNS-Name flag  to be used. If DNS-name is empty than using data from Display or Visible name. -- #
						ipToDNS = zsession.zapi.hostinterface.get(hostids=i)[0]['interfaceid']
						if len(zsession.zapi.hostinterface.get(hostids=i)[0]['dns']) == 0:
							nameForDNS = zsession.zapi.host.get(filter={'hostid':i})[0]['name']
							zsession.zapi.hostinterface.update(interfaceid=ipToDNS,dns=str(nameForDNS))
						zsession.zapi.hostinterface.update(interfaceid=ipToDNS,useip='0')
					except IndexError:
						print ('Host interface not found')
						
					try:
						# -- Checking groupmembership and assigning host to it -- #
						
						groupToAssignID = zapi.hostgroup.get(filter={'name':groupToAssign})[0]['groupid']
						if groupToAssignID:
							zsession.zapi.hostgroup.massadd(groups={'groupid':groupToAssignID},hosts={'hostid':i})
							zsession.zapi.hostgroup.massremove(groupids=tempGroupID,hostids=i)
							zsession.zapi.host.massadd(hosts=i,templates=assignedTemplate)

					except IndexError:
						print('Group to be assigned was not found for host %s' % i.encode('utf-8'))
						exit(1)
						
		else:
			print("'genericgroup' group was not found. No hosts to process.")	

	# -- Processing hosts with changed 'groupmembership' UserParemeter -- #
	elif not autoreg:
		if zbxhostname:
			try:
				zbxHostID = zsession.zabbixGetObjectID('host',zbxhostname)
			except RuntimeError:
				print("Хост с именем %s не найден в Zabbix" % zbxhostname)
				exit(1)

			if len(zsession.zapi.item.get(hostids=zbxHostID, search={'key_':'groupmembership'})) > 0:
				if not grouptoassign:
					groupToAssign = zsession.zapi.item.get(hostids=zbxHostID, search={'key_':'groupmembership'})[0]['lastvalue']
				else:
					groupToAssign = grouptoassign
					
				try:
				# -- Toggling DNS flag in host interface -- #
					ipToDNS = zsession.zapi.hostinterface.get(hostids=zbxHostID)[0]['interfaceid']
					zsession.zapi.hostinterface.update(interfaceid=ipToDNS,useip='0')
				except IndexError:
					print('Host interface was not found')
						
				try:
					hostGroups = [ group['name'] for group in zsession.zapi.hostgroup.get(hostids=zbxHostID) ]
					if groupToAssign not in hostGroups:
					# -- Deleting old group after after cheking against regex (User Workstations/Dept 000-999) -- #

						groupRegex = re.compile(u'^User\sWorkstations\/Dept\s\d{3}$')
						oldGroupToDeleteID = [ y['groupid'] for y in zsession.zapi.hostgroup.get(hostids=zbxHostID) if groupRegex.match(y['name']) ]
						groupToAssignID = zsession.zapi.hostgroup.get(filter={'name':groupToAssign})[0]['groupid']
						if groupToAssignID:
							zsession.zapi.hostgroup.massadd(groups={'groupid':groupToAssignID},hosts={'hostid':zbxHostID})	
						zsession.zapi.hostgroup.massremove(groupids=oldGroupToDeleteID,hostids=zbxHostID)
					else:	
						print("Host is already in a group")
						
				except IndexError:
					print('Host group was not found')
			else:
				print('groupmembership key was not found')

				
if __name__ == "__main__":

	parsedValues = parserFunction()
	urlCheck = re.compile('^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$')
	if parsedValues.grouptoassign:
		parsedValues.grouptoassign = parsedValues.grouptoassign.encode('utf-8')
	
	if urlCheck.match(parsedValues.url):
		if not parsedValues.zabbixhost and not parsedValues.grouptoassign:
			print('zabbixhost and grouptoassing variables are not set, preocessing autoregistered hosts.')
			groupAssigner(parsedValues.url,parsedValues.login,parsedValues.password,True)
			
		else:
			groupCheck = re.compile('^User\sWorkstations\/Dept\s\d{3}$',re.UNICODE)
			if groupCheck.match(parsedValues.grouptoassign):
				print('zabbixhost and grouptoassign values are set. Changing host group.')
				groupAssigner(parsedValues.url,parsedValues.login,parsedValues.password,False,parsedValues.zabbixhost,parsedValues.grouptoassign)
			else:
				print('Supplied group is in wrong format, must be "User Workstations/Dept 000-999"')
				
	else:
		print('Wrong Zabbix URL')