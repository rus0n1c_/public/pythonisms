﻿# -*- coding: utf-8 -*-

import argparse
import re
import time
import mailer
import logging
from zabbixConnector import zabbixConnector

logfile = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..','Logs', 'zbxDeleteHostByTrigger.log')
logging.basicConfig(filename=logfile,format='%(asctime)s -- %(levelname)s -- %(message)s',level=logging.INFO)

def parserFunction(argv=None):

	parser = argparse.ArgumentParser(description="Delete hosts from Zabbix with trigger X of age Y.")
	parser.add_argument('--url',help='Zabbix URL',required=True)
	parser.add_argument('--login',help='Login',required=True)
	parser.add_argument('--password',help='Password',required=True)
	parser.add_argument('--grouptocheck',type=lambda s: unicode(s, 'utf8'),nargs='*',help='Groups to check for trigger',required=False)
	parser.add_argument('--zabbixhost',help='Single Zabbix host with active trigger',required=False)
	parser.add_argument('--triggername',type=lambda s: unicode(s, 'utf8'),help='Trigger name needed to be checked.', required=True)
	parser.add_argument('--triggerage',type=int,help='Trigger max age', required=False)
	parser.add_argument('--onlyprint',action='store_true',help='Dry run, just show hosts with triggers',default=False)
	parsedParams = parser.parse_args(argv)
	if parsedParams.triggername and parsedParams.zabbixhost is parsedParams.triggerage:
		parser.error("--triggername requires --triggerage variable to be set")		
	return parsedParams
						
	parsedParams = parser.parse_args(argv)
	if parsedParams.grouptoassign and parsedParams.zabbixhost is None:
		parser.error("--grouptoassign requires --zabbixhost variable to be set")
		
	return parsedParams

def deleteByTrigger(host,login,password,triggername,triggerage,grouptocheck=None,zabbixhost=None,onlyprint=None):
	
	zsession = zabbixConnector(login,password,host)
	try:
		zsession.initSession()
	except RuntimeError as e:
		print(e)
		exit(1)
				
	if not grouptocheck:
		
		if zabbixhost:
			print('Single Zabbix host supplied - %s. Searching for trigger %s' % (zabbixhost, triggername.encode('utf-8')) )
			activeTriggers = zsession.zabbixGetHostTriggers(zabbixhost)
			
			if any(triggername in trigger for trigger in activeTriggers.values()):
				# .: Supplied trigger name may not be completely identical to found triggers (due to macros and variables in trigger body). Getting full trigger name if any intersection found.
				fullTriggerName = [ trigger for trigger in activeTriggers.values() if triggername in trigger ]
				
				triggerLastChange = zsession.zapi.trigger.get(triggerids=activeTriggers.keys()[activeTriggers.values().index(fullTriggerName[0])])[0]['lastchange']
				triggerAgeActive = (int(time.time()) - int(triggerLastChange))/86400
				if triggerAgeActive > triggerage:
					if not onlyprint:
						logging.info('Deleting host %s. Found trigger %s with age %s' % (zabbixhost, fullTriggerName[0].encode('utf-8'), triggerAgeActive) )
						zsession.zabbixDeleteHost(zabbixhost)
					else:
						logging.info('Found trigger %s with age %s for Zabbix host %s. "Only print" mode, doing nothing.' % (fullTriggerName[0].encode('utf-8'), triggerAgeActive, zabbixHost) )
				else:
					logging.info('Found trigger %s with age %s attached to host %s. Age is lower than supplied. Doing nothing.' % (ullTriggerName[0].encode('utf-8'), triggerAgeActive, zabbixhost) )
					exit(0)
			elif triggername not in activeTriggers.values():
				loggin.info('No triggers found. Doing nothing.')
				exit(1)
		elif not zabbixhost:
			logging.error('No host and group supplied. Exiting.')
			exit(1)
	else:
		
		# .: Searching for active triggers among all hosts and groups
		
		activeTriggers = zsession.zapi.trigger.get(search={'description':triggername},filter={'value':'1'},selectHosts='extend')

		# .: Checking all groups from 'grouptocheck' variable :.
		for selectedGroup in grouptocheck:
			assignedGroupID = zsession.zabbixGetObjectID('group',selectedGroup)
			hostsOfassignedGroupID = [i['hostid'] for i in zsession.zapi.host.get(groupids=assignedGroupID)]		

		# .: Checking that host with active trigger belongs to iterated group. If True than deleting it.
			for listOfUnreachableHosts in activeTriggers:
				for zbxUnreachableHost in listOfUnreachableHosts['hosts']:
					if zbxUnreachableHost['hostid'] in hostsOfassignedGroupID:
						fullTriggerName = listOfUnreachableHosts['description'] if triggername in listOfUnreachableHosts['description'] else triggername
						triggerLastChange = listOfUnreachableHosts['lastchange']
						triggerAgeActive = (int(time.time()) - int(triggerLastChange))/86400
						if triggerAgeActive > triggerage:
							if not onlyprint:
								logging.info('Found host %s in group %s with trigger %s and trigger age %s. Age is higher than supplied, deleting host.' % (zbxUnreachableHost['name'].encode('utf-8'), selectedGroup.encode('utf-8'), fullTriggerName.encode('utf-8'), triggerAgeActive) )
								zsession.zabbixDeleteHost(zbxUnreachableHost['name'])
							else:
								logging.info('Found host %s in group %s with active trigger %s and age %s days. "Only print" mode, doing nothing' % ( zbxUnreachableHost['name'].encode('utf-8'), selectedGroup.encode('utf-8'), fullTriggerName.encode('utf-8'), triggerAgeActive )
						else:
							logging.info('Found supplied trigger %s for host %s, but its age is lower than supplied. Doing nothing.' % ( fullTriggerName.encode('utf-8'),zbxUnreachableHost['name'].encode('utf-8')) )
					else:
						logging.warning('Found active trigger for host %s, but host is not member of supplied group. Doing nothing' %  zbxUnreachableHost['name'].encode('utf-8') )
			
if __name__ == "__main__":

	parsedValues = parserFunction()
	urlCheck = re.compile('^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$')
	if urlCheck.match(parsedValues.url):
		if parsedValues.grouptocheck and parsedValues.triggername and parsedValues.triggerage and not parsedValues.zabbixhost:	
			logging.info('Groups to check supplied. Checking hosts in this groups for trigger')
			deleteByTrigger(parsedValues.url,parsedValues.login,parsedValues.password,parsedValues.triggername,parsedValues.triggerage,parsedValues.grouptocheck,onlyprint=parsedValues.onlyprint)
		elif parsedValues.zabbixhost and parsedValues.triggername and parsedValues.triggerage and not parsedValues.grouptocheck:
			logging.info('Single host supplied. Checking for trigger')
			deleteByTrigger(parsedValues.url,parsedValues.login,parsedValues.password,parsedValues.triggername,parsedValues.triggerage,zabbixhost=parsedValues.zabbixhost, onlyprint=parsedValues.onlyprint)
		elif parsedValues.zabbixhost and parsedValues.grouptocheck:
			logging.error('Both group and single host supplied. Stopping.')
	else:
		logging.error('Wrong Zabbix URL')
