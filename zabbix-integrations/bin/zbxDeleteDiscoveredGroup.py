﻿# -*- coding: utf-8 -*-

import argparse
import re
from pyzabbix import ZabbixAPI

def parserFunction(argv=None):

	parser = argparse.ArgumentParser(description="Deletes default Zabbix group for host with other groups assigned")
	parser.add_argument('--url',help='Zabbix URL',required=True)
	parser.add_argument('--login',help='Username',required=True)
	parser.add_argument('--password',help='Password',required=True)
	parser.add_argument('--grouptocheck',type=str,nargs='*',help='Group or groups where hosts are subjected to checking and deletion of default group. Space is a delimeter.',required=True)
	parser.add_argument('--zabbixhost',help='Single Zabbix host to delete from default group (optional)',required=False)	
	parsedParams = parser.parse_args(argv)
	return parsedParams
	
def deleteDefaultGroup(host,login,password,grouptocheck,singleHost='no',zbxhostname='',genericgroup='Отстойник',zabbixhost=None):

	try:
		zapi = ZabbixAPI(host)
	except InsecureRequestWarning:
		print('Conn')
	zapi.session.verify = False
	zapi.timeout = 5
	zapi.login(login,password)

	print("Zabbix API %s" % zapi.api_version())

	defaultID = zapi.hostgroup.get(filter={'name':genericgroup})[0]['groupid']
	
	# -- Processing all hosts from groups defined on 'grouptocheck' variable -- #
	
	if singleHost == 'no':
	
		for selectedGroup in grouptocheck:	
	
			assignedGroupID = zapi.hostgroup.get(filter={'name':selectedGroup})[0]['groupid']
			hostsOfassignedGroupID = [i['hostid'] for i in zapi.host.get(groupids=assignedGroupID)]
			for i in hostsOfassignedGroupID:
				hostGroups = [j['groupid'] for j in zapi.hostgroup.get(hostids=i)]
				if defaultID in hostGroups:
					zapi.hostgroup.massremove(groupids=defaultID,hostids=i)

				# -- Toggling DNS-name flag in Zabbix. If DNS-name is not set, than getting it from display name -- #

				try:
					ipToDNS = zapi.hostinterface.get(hostids=i)[0]['interfaceid']
					if len(zapi.hostinterface.get(hostids=i)[0]['dns']) == 0:
						nameForDNS = zapi.host.get(filter={'hostid':i})[0]['name']
						zapi.hostinterface.update(interfaceid=ipToDNS,dns=str(nameForDNS))
					zapi.hostinterface.update(interfaceid=ipToDNS,useip='0')
				except IndexError:
					print ('Host interface not found')

	elif singleHost == 'yes':
		
		# -- If single Zabbix host is supplied that deleting default group for it -- #

		if zabbixhost:
			hostToProcess = zapi.host.get(filter={'name':zabbixhost})
			hostGroups = [j['groupid'] for j in zapi.hostgroup.get(hostids=hostToProcess)]
			if defaultID in hostGroups:
				zapi.hostgroup.massremove(groupids=defaultID,hostids=i)
	
						# -- Toggling DNS-name flag in Zabbix. If DNS-name is not set, than getting it from display name -- #

                        try:
                                ipToDNS = zapi.hostinterface.get(hostids=hostToProcess)[0]['interfaceid']
                                if len(zapi.hostinterface.get(hostids=hostToProcess)[0]['dns']) == 0:
                                        nameForDNS = zapi.host.get(filter={'hostid':hostToProcess})[0]['name']
                                        zapi.hostinterface.update(interfaceid=ipToDNS,dns=str(nameForDNS))
                                zapi.hostinterface.update(interfaceid=ipToDNS,useip='0')
                        except IndexError:
                                print ('Host interface not found')		
	
if __name__ == "__main__":

	parsedValues = parserFunction()
	urlCheck = re.compile('^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$')
	if urlCheck.match(parsedValues.url):
		if not parsedValues.zabbixhost:
			print('zabbixhost is empty, processing all hosts from group %s' % str(parsedValues.grouptocheck))
			deleteDefaultGroup(parsedValues.url,parsedValues.login,parsedValues.password,parsedValues.grouptocheck,'no')	
		else:
			print('zabbixhost is supplied, deleting default group for host %s' % str(parsedValues.zabbixhost))
			deleteDefaultGroup(parsedValues.url,parsedValues.login,parsedValues.password,'no',parsedValues.zabbixhost,None)
				
	else:
		print('Check Zabbix host URL, aborting...')
