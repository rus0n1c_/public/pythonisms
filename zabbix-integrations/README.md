Small Zabbix interaction scripts.

1. bin/zbxDeleteDiscoveredGroup - Deletes default group for self-registered hosts.

2. bin/zbxDeleteHostByTrigger - Deletes hosts from Zabbix with trigger X of age Y.

3. bin/zbxDeleteHostDoubles - Workaround to delete 'duplicate' hosts in clustered Zabbix. 

4. bin/zbxGroupAssignment - Autoassigns host to group according to its 'groupmembership' parameter. Handy when one needs to auto assign user workstation to various departments.
