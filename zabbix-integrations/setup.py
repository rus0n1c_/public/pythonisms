from setuptools import setup

print("-------------------------------------")
setup(name="Zabbix Scripts",version="1.0", install_requires=["jira"])

packages=['zabbixConnector']
for package in packages:
	try:
		__import__(package)
	except ImportError:
		print("Package %s is not installed. Please find it and git clone from https://bitbucket.org/rus0n1c" % package)
	exit(1)

