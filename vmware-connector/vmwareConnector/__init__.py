# -*- coding: utf-8 -*-

import re
from socket import gaierror
from pyVmomi import vim
from pyVim.connect import SmartConnectNoSSL
from collections import OrderedDict

class vmwareConnector():
	def __init__(self, login, password, URL):
		self.login = login
		self.password = password
		self.URL = URL
		
	def initSession(self):
		"""
		Initial connector to VmWare
		"""
	
		self.vmwareSession = None
		try:
			self.vmwareSession = SmartConnectNoSSL(host=self.URL,user=self.login,pwd=self.password)
		except vim.fault.InvalidLogin:
			raise RuntimeError('Connection to VmWare failed : Invalid login or password')
		except gaierror: 
			raise RuntimeError('Connection to VmWare failed : Check DNS-name of VmWare vSphere')
	
	def vmwareGatherVMList(self,searchDepth=10):
		"""
		Function for getting "raw" list of all virtual machines with attributes
		searchDepth - integer, indicates depth of vm search in Virtual Machine Inventory Folders.
		""" 
	
		rawlist = {}
		
		self.searchDepth = searchDepth
		
		# -- Recursion needed, 'cause there may be nested folders with VMs -- #
		
		def vmwareRecurseThroughFolders(vm, recurseDepth=1):
			
			if hasattr(vm, 'childEntity'):
				if recurseDepth > self.searchDepth:
					return
				tmpList = vm.childEntity
				for child in tmpList:
					vmwareRecurseThroughFolders(child,recurseDepth+1)
				return

			rawlist[vm.summary.config.name] = vm.summary
			
		if not hasattr(self.vmwareSession,'content'):
			raise RuntimeError('Cannot gather VM list. Are we connected?')
		else:
			self.content = self.vmwareSession.RetrieveContent()
			for subtree in self.content.rootFolder.childEntity:
				if hasattr(subtree,'vmFolder'):
					datacenter = subtree
					folders = datacenter.vmFolder
					vms = folders.childEntity
					for vm in vms:
						vmwareRecurseThroughFolders(vm)
						
		if rawlist:
			return rawlist
		elif not rawlist:
			raise RuntimeError('Vitual Machine list is empty. Something went wrong')

	def vmwareStrip(self,charsToStrip,string):
		"""
		Small fucntion to delete extra space and new line symbols in text fields. 
		"""
		tmp = string
		
		for pattern in charsToStrip:
			tmp = re.sub(pattern,charsToStrip[pattern],tmp)
		
		return tmp
	
	def vmwareCompileAttributes(self,rawlist,customAttribs=None):
		"""
		Creates dictionary of all attributes of virtual machine.
		rawlist - list expected from 'vmwareGatherVMList' function.
		customAttribs - Dictionary. Custom text fields defined in VmWare. E.g-- {'Owner' : '201','AD Admin Group' : '402'}, where values are internal indexes in VmWare.
		"""
		
		vmachines = {}
		vmachinesHdd = {}
		vmachinesNic = {}
		vmachinesNicMAC = {}
		vmachinesNicIP = {}
		charsToStripHDD = [',',' KB']
		
		# -- 'Description' field processing. If there is more than one space symbol - replacing it with single one. If there is a new line symbol - replacing it with space symbol. Inserting 'charstToStripString' dictionary into 'vmwareStrip' function -- #
		charsToStripString = {'\n{1,}':' ','\s{2,}':' '}
		
		if not rawlist:
			raise RuntimeError("Failed to compile VM attributes. Are we connected?")
		else:
			
			# -- Excluding virtual machine templates from VM List. Checking 'Template' word in vm name (in case admins forgot to convert such machines into template) and template flag in virtual machine properties. -- #
			
			templates = [ template for template in rawlist if rawlist[template].vm.config.template or 'Template' in rawlist[template].vm.config.name ]
			for pop in templates:
				del rawlist[pop]
				
			for cursor in rawlist:

				# -- Getting attributes. HDDs. Stripping commas and size, leaving just pure digits. Soring by size. -- #
				
				vmachinesHdd[cursor] = []
				for hdds in rawlist[cursor].vm.config.hardware.device:
					if 'Hard disk' in hdds.deviceInfo.label:
						dirty = hdds.deviceInfo.summary
						cleaned = dirty.translate(None,''.join(charsToStripHDD))
						calculated = int(cleaned)/1024/1024
						vmachinesHdd[cursor].append(calculated)
					vmachinesHdd[cursor] = sorted(vmachinesHdd[cursor],reverse=True)
					
				# -- Getting attributes. Network adapters. Finding IP-addresses by MAC-addresses of NIC Adapters. -- #
								
				vmachinesNic[cursor] = {}
				vmachinesNicMAC[cursor] = {}
				vmachinesNicIP[cursor] = {}
				for nics in rawlist[cursor].vm.config.hardware.device:
					if 'Network adapter' in nics.deviceInfo.label:
 						vmachinesNic[cursor][nics.deviceInfo.label] = nics.macAddress
					
					vmachinesNicMAC[cursor] = OrderedDict(sorted(vmachinesNic[cursor].iteritems(), key=lambda x: x))
					
					for vnic in rawlist[cursor].vm.guest.net:
						vmachinesNicIP[cursor][vnic.macAddress] = vnic.ipAddress
				
				# -- Getting attributes. IPv4 addresses. Excluding IPv6 Link-Local. -- #
				
				ipv4addresses = {}
				for ip in rawlist[cursor].vm.guest.net:
					ipv4addresses[ip.macAddress] = ''
					for ipv6 in ip.ipAddress:
							if ipv6.startswith('fe'):
								ip.ipAddress.remove(ipv6)
								ipv4addresses[ip.macAddress] = ip.ipAddress
							else:
								ipv4addresses[ip.macAddress] = ip.ipAddress
				
				ipv4 = []
				for ip in ipv4addresses:
					if ipv4addresses[ip]:
						ipv4 = ipv4 + list(ipv4addresses[ip])				
				
				# -- Getting  attributes. VLANs. Assuming VLAN Name in VmWare dvSwitch is 'VLAN-<number>, e.g. VLAN-1234. Extracing only digits. -- #
								
				vlans = [ nic.network if nic.macAddress in ipv4addresses else '' for nic in rawlist[cursor].vm.guest.net]
				
				vlansFiltered = []
				for vlan in vlans:
					if isinstance(vlan,basestring):
						vlansFiltered.append(filter(str.isdigit, vlan))
					elif isinstance(vlan,unicode):
						vlansFiltered.append(re.findall('\d+',vlan)[0])

				# -- Compiling final dictionary with vm attribute -- #			

				print('Processing Virtual Machine : %s' % (cursor))
				vmachines[cursor] = {
											'DNS' : '' if not rawlist[cursor].vm.summary.guest.hostName else rawlist[cursor].vm.summary.guest.hostName,
											'OS' : rawlist[cursor].vm.summary.config.guestFullName,
											'VRAM': str(rawlist[cursor].config.memorySizeMB/1024),
											'VCPU': str(rawlist[cursor].config.numCpu),
											'DESC': self.vmwareStrip(charsToStripString,rawlist[cursor].config.annotation),
											'VHDD': ' '.join(str(i) for i in vmachinesHdd[cursor]),
											'MAC' : ', '.join(ipv4addresses.keys()),
											'IP': ', '.join(ipv4),
											'VLAN': ', '.join(vlansFiltered),
											'POWERSTATUS': rawlist[cursor].vm.runtime.powerState
										}
				if customAttribs and isinstance(customAttribs, dict):
					for key in customAttribs.keys():
						vmachines[cursor].update({key : ''.join([attr.value for attr in rawlist[cursor].customValue if attr.key == int(customAttribs[key])]) })
						
		return vmachines
					