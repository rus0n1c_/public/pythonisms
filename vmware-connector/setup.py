from setuptools import setup

setup(name='vmware-connector', 
	  version='1.0',
	  description='VmWare API Connector',
	  url='https://bitbucket.org/rus0n1c/vmware-connector',
	  author='Alexey U.Gagarin',
	  author_email='bitbucket@incodewetrust.net',
	  license='MIT',
	  packages=['vmwareConnector'],
	  install_requires=['pyVmomi'],
	  zip_safe=False)
