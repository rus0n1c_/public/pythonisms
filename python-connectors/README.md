## Handy connectors to various systems

* connectors/jiraConnector.py -- Connector to JIRA

* connectors/ldapConnector.py -- Connector to LDAP/Active Directory

* connectors/mailConnector.py -- Connector for sending e-mail messages and/or attachments 


Note that ```jira```, ```ldap3``` and ```requests``` must be installed. This is resolved automatically in ```setup.py```. Also, ```Python 3``` must be used.

## Installation
```bash
cd /tmp
git clone https://git.ringcentral.com/sysops/python-connectors
cd python-connectors
python3 setup.py install
```
## Usage
```python
from connectors import jiraConnector
from connectors import ldapConnector
from connectors import mailConnector
```
