import ldap3
import json
import time


class ldapGeneralException(Exception):
    """
    Base LDAP excepton class
    """
    def __init__(self, ldapreason):

        self.reason = {'ldap_result': 'General LDAP Failure',
                       'ldap_result_code': ldapreason.result,
                       'ldap_result_message': ldapreason.message}
        super().__init__(self.reason)


class ldapConnectionNotSecure(Exception):

    def __init__(self):
        self.reason = {'ldap_result': 'Connection to LDAP-server is not SSL/TLS-protected',
                       'ldap_reason': 'SSL/TLS connection is required for requested action'}
        super().__init__(self.reason)


class ldapPasswordChangeFailed(Exception):

    def __init__(self, ldapreason):
        self.reason = {'ldap_result': 'Failed to change password of LDAP account',
                       'ldap_result_message': ldapreason.message}
        super().__init__(self.reason)


class ldapInvalidCredentials(Exception):

    def __init__(self):
        self.reason = {'ldap_result': 'Failed to connect to LDAP-server',
                       'ldap_reason': 'Invalid credentials'}
        super().__init__(self.reason)


class ldapBindError(Exception):

    def __init__(self, ldapreason):
        self.reason = {'ldap_result': 'Failed to bind to LDAP-server',
                       'ldap_result_code': ldapreason.result,
                       'ldap_result_message': ldapreason.message}
        super().__init__(self.reason)


class ldapCommunicationError(Exception):

    def __init__(self, ldapreason):
        self.reason = {'ldap_result': 'Failed to bind to LDAP-server',
                       'ldap_result_code': 'No connection',
                       'ldap_result_message': ldapreason}
        super().__init__(self.reason)


class ldapSocketError(Exception):

    def __init__(self, ldapreason):
        self.reason = {'ldap_result': 'Failed to bind to LDAP-server',
                       'ldap_result_code': 'No connection',
                       'ldap_result_message': ldapreason}
        super().__init__(self.reason)


class ldapNotEnoughData(Exception):

    def __init__(self):
        self.reason = {'ldap_result': 'Failed to create LDAP account',
                       'ldap_result_message': 'At least sAMAccountName and userprincipalname must be supplied'}
        super().__init__(self.reason)


class ldapUserExists(Exception):

    def __init__(self):
        self.reason = {'ldap result': 'Failed to create LDAP account',
                       'ldap_result_code': 68,
                       'ldap_result_message': 'Account already exists'}
        super().__init__(self.reason)


class ldapInsufficientAccessRights(Exception):

    def __init__(self, ldapreason):
        self.reason = {'ldap_result': 'No rights for account creation',
                       'ldap_result_code': ldapreason.result,
                       'ldap_result_message': ldapreason.message}
        super().__init__(self.reason)


class ldapObjectNotFound(Exception):

    def __init__(self, ldapreason):
        self.reason = {'ldap_result': 'LDAP path or object was not found',
                       'ldap_result_code': ldapreason.result,
                       'ldap_result_message': ldapreason.message}
        super().__init__(self.reason)


class ldapUnsupportedChangeMethod(Exception):

    def __init__(self, ldapreason):
        self.reason = {'ldap_result': 'Failed to modify LDAP attribute',
                       'ldap_reason': 'Modification method must be one of %s' % ldapreason}
        super().__init__(self.reason)


class ldapWrongFilter(Exception):

    def __init__(self, ldapreason=None):
        self.reason = {'ldap_result': 'Failed to modify LDAP attribute',
                       'ldap_reason': '%s' % (ldapreason if ldapreason else 'Wrong LDAP filter format')}
        super().__init__(self.reason)


class ldapConnector():
    """
    Abstract class to work with LDAP
    """

    def __init__(self, lhost, lusername, lpassword, use_ssl=True, **kwargs):
        """
        Append other options to ldap3.Server from kwargs.
        Please see ldap3.Server help to find out what options to use
        """
        ldapOptions = ldap3.Server(lhost, use_ssl=use_ssl, **kwargs)
        self.ldapConn = ldap3.Connection(ldapOptions, user=lusername, password=lpassword, raise_exceptions=True)

        try:
            self.ldapConn.bind()
        except ldap3.core.exceptions.LDAPInvalidCredentialsResult:
            raise ldapInvalidCredentials()
        except ldap3.core.exceptions.LDAPBindError as e:
            raise ldapBindError(e)
        except ldap3.core.exceptions.LDAPCommunicationError as e:
            raise ldapCommunicationError(e.args[0])
        except ldap3.core.exceptions.LDAPSocketOpenError as e:
            raise ldapSocketError(e.args[0])

    def ldapEndSession(self, lconn):
        """
        Unbind from LDAP
        """

        try:
            lconn.unbind()
        except Exception as e:
            raise ldapGeneralException(e)

    def ldapSearchByFilter(self, lconn, lbasedn, lfilter, **kwargs):
        """
        Search users, groups or other data with 'lfilter'
        Pass other options with **kwargs to .search method of ldap3
        """

        try:
            lconn.search(search_base=lbasedn, search_filter=lfilter, **kwargs)
            if lconn.result['result'] == 0 and lconn.entries:
                return lconn.entries
            elif lconn.result['result'] == 0 and not lconn.entries:
                return None
        except ldap3.core.exceptions.LDAPSocketOpenError as e:
            raise ldapSocketError(e.args[0])
        except ldap3.core.exceptions.LDAPAttributeError as e:
            raise ldapWrongFilter()
        except ldap3.core.exceptions.LDAPInvalidFilterError as e:
            raise ldapWrongFilter(lfilter)

    def ldapManipulateAttribute(self, lconn, lcname, lattrname, lattrvalue, lmethod):
        """
        Manipulate LDAP object attribute, e.g, userAccountControl
        See https://ldap3.readthedocs.io/modify.html for available kinds of change.
        """
        availableMethods = ['MODIFY_ADD',
                            'MODIFY_DELETE',
                            'MODIFY_INCREMENT',
                            'MODIFY_REPLACE']

        if lmethod not in availableMethods:
            raise ldapUnsupportedChangeMethod(availableMethods)

        try:
            time.sleep(5)  # Sometimes LDAP-server returns 53 WILL_NOT_PERFORM due to changes requested to quickly. 
            lconn.modify(lcname, {lattrname: [(lmethod, lattrvalue)]})
            if lconn.result['result'] == 0:
                return True
            elif lconn.result['result'] != 0:
                raise ldapGeneralException(lconn.result['message'])
        except Exception as e:
            raise ldapGeneralException(e)

    def ldapUserAccountChangePassword(self, lconn, lcname, l_newpassword, l_oldpassword=None):
        """
        Change password of the AD account. Required LDAP over SSL connection.
        lcname -- Full CN path. Example: cn=opsuser, cn=Users, dc=ringcentral, dc=com
        l_newpassword -- New user password
        l_oldpassword -- Old user password. Needed if password is changed by user itself, not by service/admin account
        """

        if not lconn.server.ssl or not lconn.server.tls:
            raise ldapConnectionNotSecure()

        try:
            lconn.extend.microsoft.modify_password(lcname, l_newpassword, l_oldpassword)
            if lconn.result['result'] == 0:
                return True
            if lconn.result['result'] != 0:
                raise ldapPasswordChangeFailed(lconn.result['message'])
        except Exception as e:
            raise ldapGeneralException(e)
        except ldap3.core.exceptions.LDAPUnwillingToPerformResult as e:
            raise ldapGeneralException(e)

    def ldapUserAccountCreateNew(self, lconn, lcname, lpassword, **kwargs):
        """
        Create LDAP User.
        lcname -- Full CN path. Example: cn=opsuser, cn=Users, dc=ringcentral, dc=com
        lpassword -- Password for user to be created
        **kwargs -- Additional LDAP attributes dictionary. See https://ldap3.readthedocs.io/add.html for examples.

        To create user account, we need at least sAMAccountName and principalname.
        """

        if not kwargs.get("samaccountname") or not kwargs.get("userprincipalname"):
            raise ldapNotEnoughData()

        attributes = {'objectClass': ['User', 'top', 'posixGroup']}

        for attrname, attrvalue in kwargs.items():
            attributes[attrname] = attrvalue

        try:
            lconn.add(lcname, attributes=attributes)
            time.sleep(5)  # Sometimes LDAP-server returns 53 WILL_NOT_PERFORM due to changes requested to quickly.
        except ldap3.core.exceptions.LDAPInsufficientAccessRightsResult as e:
            raise ldapInsufficientAccessRights(e)

        except ldap3.core.exceptions.LDAPNoSuchObjectResult as e:
            raise ldapObjectNotFound(e)

        # -- LDAP Response Code 68 indicates that user exists already -- #
        if lconn.result['result'] == 68:
            raise ldapUserExists()

        elif lconn.result['result'] == 0:
            ldapResult = self.ldapUserAccountChangePassword(lconn, lcname, lpassword)
            time.sleep(5)  # Sometimes LDAP-server returns 53 WILL_NOT_PERFORM due to changes requested to quickly.

            # -- If we want user to change his password after first login, we need to reset pwdLastSet to 0 -- #
            if ldapResult:

                # -- Finally, making AD account active -- #
                self.ldapManipulateAttribute(lconn, lcname, 'userAccountControl', 512, 'MODIFY_REPLACE')

                return True
            else:
                return False

    def ldapUserAccountDelete(self, lconn, lcname):
        """
        Delete LDAP object (in this case, user).
        lcname -- Full CN path. Example: cn=opsuser, cn=Users, dc=ringcentral, dc=com
        lpassword -- Password for user to be created
        **kwargs -- Additional LDAP attributes dictionary. See https://ldap3.readthedocs.io/delete.html for examples.
        """

        try:
            lconn.delete(lcname)
            time.sleep(5)  # Sometimes LDAP-server returns 53 WILL_NOT_PERFORM due to changes requested to quickly.
        except ldap3.core.exceptions.LDAPInsufficientAccessRightsResult as e:
            raise ldapInsufficientAccessRights(e)

        except ldap3.core.exceptions.LDAPNoSuchObjectResult as e:
            raise ldapObjectNotFound(e)

        except Exception as e:
            raise ldapGeneralException(e)

        if lconn.result['result'] == 0:
            return True

        if lconn.result['result'] != 0:
            return False

    def ldapAddUserToGroup(self, lconn, lcname, lgroup):
        """
        Add user to LDAP group
        """

        try:
            lconn.extend.microsoft.add_members_to_groups(lcname, lgroup)
            if lconn.result['result'] == 0:
                return True
            else:
                return False
        except Exception as e:
            raise ldapGeneralException(e)

    def ldapDeleteUserFromGroup(self, lconn, lcname, lgroup):
        """
        Remove user from LDAP group
        """

        try:
            result = lconn.extend.microsoft.remove_members_from_groups(lcname, lgroup)
            if lconn.result['result'] == 0:
                return True
            else:
                return False
        except Exception as e:
            raise ldapGeneralException(e)
