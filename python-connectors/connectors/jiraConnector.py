import jira
import sys
import re


class jiraGeneralException(Exception):
    """
    Base Jira exception class
    """

    def __init__(self, statusCode, statusReason):

        self.reason = {'jira_http_result': statusCode,
                       'jira_http_reason': statusReason}
        super().__init__(self.reason)


class jiraInvalidURL(Exception):

    def __init__(self):

        self.reason = {'jiraresult': 'Failed to parse Jira URL',
                       'jirareason': 'URL didn\'t pass regex validation'}
        super().__init__(self.reason)


class jiraNoSuchIssue(Exception):

    def __init__(self):

        self.reason = {'jiraresult': 'Jira issue was not found',
                       'jirareason': 'Is JQL or issue number valid?'}
        super().__init__(self.reason)


class jiraConnector():
    """
    Abstract class to work with JIRA
    """

    def __init__(self, jurl, jusername, jpassword, jretries=0):

        regex = re.compile("https?:\\/{2}([a-zA-Z0-9-]+\\.){1,3}\\w{2,5}")
        if not re.match(regex, jurl):
            raise jiraInvalidURL()

        try:
            self.jconn = jira.JIRA(jurl, basic_auth=(jusername, jpassword), max_retries=jretries)
        except jira.exceptions.JIRAError as e:
            raise jiraGeneralException(e.status_code,
                                       e.text)

    def jiraRunJQLQuery(self, jconn, jqlquery):
        """
        Search for Jira issues using JQL query
        Also, predefined filter ID may be used in jqlQuery, e.g. 'filter=12345'
        """

        try:
            jiraSearchResult = jconn.search_issues(jqlquery)

            if not jiraSearchResult:
                return None
            else:
                return jiraSearchResult
        except jira.exceptions.JIRAError as e:
            raise jiraGeneralException(e.status_code,
                                       e.text)

    def jiraIssueGetReporter(self, jconn, jissue):
        """
        Get reporter of the 'jissue'
        """

        try:

            raw = self.jiraRunJQLQuery(jconn, "issue = %s" % jissue)
            jiraIssueReporter = raw[0].fields.reporter.name
            return jiraIssueReporter

        except IndexError:
            raise jiraNoSuchIssue()

    def jiraIssueAddLabel(self, jconn, jissue, jlabel):
        """
        Pin label to supplied issue
        """

        try:
            issueInstance = jconn.issue(jissue)
            issueInstance.fields.labels.append(jlabel)
            issueInstance.update(fields={"labels": issueInstance.fields.labels})
        except jira.exceptions.JIRAError as e:
            raise jiraGeneralException(e.status_code,
                                       e.text)
        except (AttributeError, IndexError) as e:
            raise jiraNoSuchIssue()

    def WasIssueCloned(self, jconn, jissue):
        """
        Verification, that issue was cloned from another issue
        """
        try:
            issue = jconn.issue(jissue, expand='changelog')
            for history in issue.changelog.histories:
                for item in history.items:
                    if item.field == 'Link':
                        if item.toString is not None and 'This issue Clones' in item.toString:
                            return True
            return False
        except jira.exceptions.JIRAError as e:
            raise jiraGeneralException(e.status_code,
                                       e.text)

    def jiraIssueChangeAssignee(self, jconn, jissue, jassignee):
        """
        Change assignee of the issue
        Expects jissue as a string with ticket number, e.g. OPS-123456
        """

        try:
            jconn.assign_issue(jissue,
                               jassignee)

        except jira.exceptions.JIRAError as e:
            raise jiraGeneralException(e.status_code,
                                       e.text)

    def jiraIssueComment(self, jconn, jissue, jcomment, visibility=None):
        """
        Leave a comment in issue
        'visibility' - {'type':<type, 'value': <value>} dictionary, where 'type' is either 'role' or 'group'.
        """

        try:
            if not visibility:
                jconn.add_comment(jissue, jcomment)
            if visibility:
                jconn.add_comment(jissue, jcomment, visibility)
        except jira.exceptions.JIRAError as e:
            raise jiraGeneralException(e.status_code,
                                       e.text)

    def jiraIssueClose(self, jconn, jissue, jcomment=None, jcloseids='31'):
        """
        Closes Jira issue with or without comment.
        'jcloseid' is usually 31 to close issue. May be also a comma-separated states.
        """

        states = jcloseids.split(',')

        try:
            if jcomment:
                self.jiraIssueComment(jconn,
                                      jissue,
                                      jcomment)

            for state in states:
                jconn.transition_issue(jissue, state)
        except jira.exceptions.JIRAError as e:
            raise jiraGeneralException(e.status_code,
                                       e.text)

    def jiraRenameIssue(self, jconn, jissue, jissuename):
        """
        Change issue title
        """

        try:
            issue = jconn.issue(jissue)
            issue.update(fields={"summary": jissuename})
        except jira.exceptions.JIRAError as e:
            raise jiraGeneralException(e.status_code,
                                       e.text)
