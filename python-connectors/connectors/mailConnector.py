# -*- coding: utf-8 -*-
from __future__ import absolute_import

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.header import Header
from email import encoders
from smtplib import SMTP_SSL
from smtplib import SMTP
from smtplib import (SMTPAuthenticationError,
                     SMTPHeloError,
                     SMTPConnectError,
                     SMTPRecipientsRefused,
                     SMTPSenderRefused,
                     SMTPException)

import os
import time
import sys


class mailerGeneralException(Exception):
    """
    Base mailer exception class
    """
    def __init__(self, smtpText, smtpCode, smtpReason):

        self.reason = {'mail_result': smtpText,
                       'mail_result_code': smtpCode,
                       'mail_result_message': smtpReason}
        super().__init__(self.reason)


class mailerSMTPAuthenticationFailure(mailerGeneralException):
    pass


class mailerInstaniationError(mailerGeneralException):
    pass


class mailerIncorrectAttachment(Exception):
    def __init__(self, attachmentPath, attachmentReason):
        self.reason = {'mail_result': 'Attachment error',
                       'mail_result_code': None,
                       'mail_result_message': 'Couldn\'t load attachment from file %s. Error: %s' % (attachmentPath, attachmentReason) }
        super().__init__(self.reason)


class mailerIncorrectParameter(Exception):

    def __init__(self, passedParameter, allowedParameters):
        self.reason = {'mail_result': 'Incorrect parameter',
                       'mail_result_code': None,
                       'mail_result_message': '%s is passed, but must be one of %s'
                       % (passedParameter,
                          allowedParameters)}
        super().__init__(self.reason)


class mailerInsufficientParameters(Exception):

    def __init__(self, neededParameters):
        self.reason = {'mail_result': 'Not enough parameters',
                       'mail_result_code': None,
                       'mail_result_message': "%s parameters need to be set as well" % neededParameters}
        super().__init__(self.reason)


class mailer():
    """
    Main mailer class.
    """
    def __init__(self,
                 smtpServer,
                 smtpRecipients,
                 smtpSender,
                 smtpConnectonMethod='plain',
                 smtpAttach='',
                 smtpBodyType='plain',
                 smtpAuthMethod='plain',
                 **smtpArgs):

        self.server = smtpServer
        self.recipients = [ r for r in smtpRecipients ] if isinstance(smtpRecipients, list) else smtpRecipients
        self.sender = smtpSender
        self.connMethod = smtpConnectonMethod
        self.attaches = [ at for at in smtpAttach ] if isinstance(smtpAttach, list) else [ smtpAttach ]
        self.btype = smtpBodyType
        self.authMethod = smtpAuthMethod
        self.subject = smtpArgs.get('smtpSubject') if smtpArgs.get('smtpSubject') else None

        if self.btype not in ('html', 'plain'):
            raise mailerIncorrectParameter(self.btype, ['html', 'plain'])
        if self.connMethod not in ('starttls', 'ssl', 'plain'):
            raise mailerIncorrectParameter(self.connMethod, ['starttls', 'ssl', 'plain'])
        if self.authMethod not in ('password', 'plain'):
            raise mailerIncorrectParameter(self.authMethod, ['password', 'plain'])
        if self.authMethod == 'password' and not smtpArgs.get('login') or self.authMethod == 'password' and not smtpArgs.get('password'):
            raise mailerInsufficientParameters(['login', 'password'])
        else:
            self.login = smtpArgs.get('login')
            self.password = smtpArgs.get('password')

        # -- Unlikely, but non-standard SMTP/SSL SMTP/TLS SMTP port may be used -- #
        self.port = smtpArgs.get('smtpPort') if smtpArgs.get('smtpPort') else None

        # -- We pass smtpDebug=<level> if we need to find out what went wrong -- #
        if smtpArgs.get('smtpDebug'):
            self.debuglevel = smtpArgs.get('smtpDebug')
        else:
            self.debuglevel = 0

    def mailerPrepareConnection(self):
        """
        Prepare connection before actually sending message
        Sets connection method, introduces us to server and logs in
        """
        try:
            if self.connMethod == 'starttls':
                self.port = self.port if self.port else '25'
                self.conn = SMTP(self.server)
                self.conn.connect(self.server, self.port)
                self.conn.set_debuglevel(self.debuglevel) if self.debuglevel else self.conn.set_debuglevel(0)
                self.conn.ehlo()
                self.conn.starttls()
                self.conn.ehlo()
            elif self.connMethod == 'ssl':
                self.port = self.port if self.port else '465'
                self.conn = SMTP_SSL(self.server)
                self.conn.connect(self.server, self.port)
                self.conn.set_debuglevel(self.debuglevel) if self.debuglevel else self.conn.set_debuglevel(0)
                self.conn.ehlo()
            elif self.connMethod == 'plain':
                self.port = self.port if self.port else '25'
                self.conn = SMTP(self.server)
                self.conn.connect(self.server, self.port)
                self.conn.set_debuglevel(self.debuglevel) if self.debuglevel else self.conn.set_debuglevel(0)
                self.conn.ehlo()

            if self.authMethod == 'password':
                self.conn.login(self.login, self.password)
        except SMTPAuthenticationError as e:
            raise mailerSMTPAuthenticationFailure('Authentication failure',
                                                  e.smtp_code,
                                                  e.smtp_error)
        except SMTPException as e:
            raise mailerGeneralException('General SMTP error',
                                         e.smtp_code,
                                         e.smtp_error)

    def mailerPrepareMessage(self, textContent=None):
        """
        Prepare message before actually sending it
        self.attaches is supposed to be a list of filepaths for valid files to send
        """

        separator = ', '

        self.msg = MIMEMultipart()
        self.msg['Subject'] = self.subject
        self.msg['From'] = self.sender
        self.msg['To'] = separator.join(self.recipients) if isinstance(self.recipients, list) else self.recipients

        # -- Inserting text into e-mail message if any -- #
        self.text = textContent if textContent else None
        if self.text:
            self.msg.attach(MIMEText(self.text, self.btype, 'utf-8'))

        # -- Preparing attaches if any -- #
        try:
            if self.attaches[0] != '':
                for attach in self.attaches:
                    body = MIMEBase('application', 'octet-stream')
                    body.set_payload( open(attach, 'rb').read() )
                    encoders.encode_base64(body)
                    body.add_header('Content-Disposition',
                                    'attachment; filename="{}"'.format(os.path.basename(attach)) )
                    self.msg.attach(body)
        except FileNotFoundError:
            raise mailerIncorrectAttachment(attach, 'No such file or directory')
        except IsADirectoryError:
            raise mailerIncorrectAttachment(attach, 'Directory passed, not file')
        except AttributeError as e:
            raise mailerInstaniationError('Not all attributes are initialized. Have you instaniated \'mailerPrepareConnection\' method?')

    def mailerSendMessage(self):
        """
        Sends prepared message
        """

        try:

            self.conn.sendmail(self.sender, self.recipients, self.msg.as_string())

        except (SMTPHeloError,
                SMTPRecipientsRefused,
                SMTPSenderRefused,
                SMTPDataError,
                SMTPNotSupportedError) as e:
            raise mailerGeneralException('An error occured during message sending',
                                         e.smtp_code,
                                         e.smtp_error)

        except AttributeError as e:
            raise mailerInstaniationError('Not all attributes are initialized. Have you instaniated \'mailerPrepareConnection\' method and \'mailerPrepareMessage\'?')

    def mailerFinishConnection(self):
        """
        Finishes connection
        """

        try:
            self.conn.close()
        except Exception:
            pass
