from setuptools import setup, find_packages

setup(
    name="python-connectors",
    version="0.3",
    packages=find_packages(),
    include_package_data=True,

    install_requires=['jira>=2.0.0',
                      'ldap3>=2.5',
                      'requests>=2.20.0'
                      ],

    data_files=[],

    license='GNU GPL',
    author="Alexey Gagarin",
    author_email="alexey.gagarin@nordigy.ru",
    description="Handy scripts to be used in other automations",
    project_urls={
        "Source Code": "https://git.ringcentral.com/sysops/python-connectors",
    }
)
