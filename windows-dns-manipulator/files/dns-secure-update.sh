#!/bin/bash

# -- Setting extglob for additional pattern matching -- #
shopt -s extglob
shopt -s nocasematch

hlp () {
cat <<EOF
$(tput setaf 2)$(basename $BASH_SOURCE)$(tput sgr0) $(tput setaf 3) [-a ACTION] [-s SOURCE] [-t TYPE] [-d DESTINATION] [-f] [-l TTL] [-r] [-v] [-k] $(tput sgr0)

Securely create/update/delete DNS-records on Windows DNS-servers via Kerberos GSS-TSIG. Based on https://habr.com/post/265969

Also, a $(tput setaf 3)\$DNS_AD_SERVER$(tput sgr0) environment variable is expected (a server to which a connection is made).

$(tput setaf 3)-a <action> $(tput sgr0) - Add or delete DNS-record. "add" and "delete" are expected as action, respectively.
$(tput setaf 1)WARNING!$(tput sgr0) If --force is specified and an existing record is found, the script will erase it completely first!

$(tput setaf 3)-s <source record>$(tput sgr0) - An FQDN to be created.
$(tput setaf 3)-t <record_type>$(tput sgr0) - A type of record to be created. $(tput setaf 3)A/CNAME/SRV/TXT/PTR$(tput sgr0) records are supported
$(tput setaf 3)-d <destination value>$(tput sgr0) - A value of FQDN (either IP-address or string depending on the type of record). 
If several comma-separated IP-addresses are specified, the script will try to create a single record with multiple values at once.
May be omitted if -d flag is specified (in that case, all values for record are deleted, e.g. 1.1.1.1 and 2.2.2.2 for record test.domain.tld. )

$(tput setaf 3)-l <value>$(tput sgr0) - a TTL of a record to specify. Defaults to 3600.
$(tput setaf 3)-f$(tput sgr0) - If specified, the script will forcefully replace existing DNS-record if it finds one; otherwise, it will report about it and exit
$(tput setaf 3)-r$(tput sgr0) - (Only for A-type records) If specified, a corresponding PTR-record will be created/deleted along with A-record one.
$(tput setaf 3)-k$(tput sgr0) - If specified, the script expects $(tput setaf 1)DNS_KRB5_KEYTAB$(tput sgr0) and $(tput setaf 1)DNS_PRINCIPAL_NAME$(tput sgr0) environment variables to be set to use kinit. 
It assumes that TGT has been already received beforehand otherwise.
$(tput setaf 3)-v$(tput sgr0) - Debug mode.

Example №1: Create A-record with PTR for FQDN my-dns-update.domain.tld pointing to 8.8.8.8 with TTL 86400 and erase existing records if any;

$(basename $BASH_SOURCE) -a add -s my-dns-update.domain.tld. -d 8.8.8.8 -t A -f -r -l 86400

Example №2: Create a single A-record all-my-hosts.domain.tld with 3 IP-addresses (1.1.1.1, 2.2.2.2 and 3.3.3.3) with TTL 60

$(basename $BASH_SOURCE) -a add -s all-my-hosts.domain.tld. -d 1.1.1.1,2.2.2.2,3.3.3.3 -t A -r -l 60

EOF
exit 1
}

TTL=3600 # Default TTL if -t is not specified
NSUPDATE="nsupdate -g" # If debug mode is not enabled, we run nsupdate without debugging
KEYTAB="no"      # Just a placeholder to pass ENV var check. If -k is specified, the ${DNS_KRB5_KEYTAB} must be a valid path to keytab
PRINCIPAL="no"   # Same as KEYTAB, but ${DNS_PRINCIPAL_NAME} is expected instead

get_tgt() {
    # Get Ticket Granting Ticket with $1 keytab issued for $2 principal
    kinit -k -t "$1" "$2" > /dev/null || { exit 1; }
}

function check_arg(){
    # Check OPTARG is not empty for getopt options
    if [[ $2 == -* ]]; then 
        echo "Option $1 requires an argument" >&2
        exit 1
    fi
}

# Parsing arguments
while getopts ":a:s:d:t:l:frv" opt; do
    case $opt in
        a) check_arg "-a" "$OPTARG"; ACTION="$OPTARG"; ;;
        s) check_arg "-s" "$OPTARG"; SOURCE="$OPTARG"; ;;
        d) check_arg "-d" "$OPTARG"; DESTINATION="$OPTARG"; ;;
        t) check_arg "-t" "$OPTARG"; TYPE="$OPTARG"; ;;
        l) check_arg "-l" "$OPTARG"; TTL="$OPTARG"; ;;
        r) PTR_REQUESTED="True"; ;;
        f) FORCE_REQUESTED="True"; ;;
        k) USE_KEYTAB="yes"; KEYTAB=${DNS_KRB5_KEYTAB_PATH}; PRINCIPAL=${DNS_PRINCIPAL_NAME}; ;;
        v) NSUPDATE="nsupdate -gd"; ;;
    esac
done

# Check required command-line arguments are specified
if [[ -z "${ACTION}" || -z "${SOURCE}" || -z "${TYPE}" ]]; then
    echo -e "$(tput setaf 1)\nOne of the required inputs is missing, please, verify your input.\n$(tput sgr0)" >&2
    hlp
fi

# Check environment variables
EXPECTED_ENV_VARS=( DNS_AD_SERVER KEYTAB PRINCIPAL )
EXPECTED_EXECUTABLES=( nsupdate dig kinit)

for ENV_VAR in ${EXPECTED_ENV_VARS[*]}; do
    if [[ -z ${!ENV_VAR} ]]; then
        echo "$(tput setaf 1)${ENV_VAR} environment variable is not set. Could not proceed.$(tput sgr0)" >&2
        exit 1
    fi
done

# Check executables
for EXECUTABLE in ${EXPECTED_EXECUTABLES[*]}; do
    command -v ${EXECUTABLE} >/dev/null  2>&1 || { echo "${EXECUTABLE} is not found. Could not proceed." >&2; exit 1; }
done

# Processing the DNS-record actions
case $ACTION in
            add)

                # Start with A-records as they are the most used ones and require special treatment due to possible PTR update as well
                if [[ ${TYPE} =~ ^A$ ]]; then

                    RESULT=$(dig +nocookie +short -t ${TYPE} @${DNS_AD_SERVER} ${SOURCE})

                    if [[ ! -z ${RESULT} && -z ${FORCE_REQUESTED} ]]; then
                        echo "An existing record found (${RESULT}), but no --force flag is supplied. Exiting." >&2
                        exit 1
                    fi

                    # If no record found at all, we simply add it
                    if [[ -z ${RESULT} ]]; then
                        echo "Existing record ${SOURCE} wasn't found, will create new."
                        MULTIRECORD_HOSTS=( $(echo "${DESTINATION}" | tr ',' ' ' ) )
                        NSUPDATE_COMMAND="server ${DNS_AD_SERVER}\n"
                        for host in ${MULTIRECORD_HOSTS[*]}; do 
                            NSUPDATE_COMMAND+="update delete ${SOURCE} ${TYPE} ${host}\nupdate add ${SOURCE} ${TTL} ${TYPE} ${host}\n"
                        done
                        NSUPDATE_COMMAND+="send"
                    fi

                    # If record is found and --force flag is specified, we forcefully delete the existing one (even if it has multiple IP-addresses masspped)
                    if [[ ! -z ${RESULT} && ! -z ${FORCE_REQUESTED} ]]; then
                        echo "An existing record found (${RESULT} and --force flag is specified, the record will be removed COMPLETELY!"
                        NSUPDATE_COMMAND="server ${DNS_AD_SERVER}\nupdate delete ${SOURCE} ${TYPE}\nsend\n"
                        MULTIRECORD_HOSTS=( $(echo "${DESTINATION}" | tr ',' ' ' ) )
                        for host in ${MULTIRECORD_HOSTS[*]}; do 
                            NSUPDATE_COMMAND+="update delete ${SOURCE} ${TYPE} ${host}\nupdate add ${SOURCE} ${TTL} ${TYPE} ${host}\n"
                        done
                        NSUPDATE_COMMAND+="send"
                    fi

                    # A PTR maybe requested to add as well
                    if [[ ! -z ${PTR_REQUESTED} ]]; then

                        # Fail-safe check to make sure user hasn't submitted illegal value, only a single IP-address is expected if PTR is expected to be updated as well
                        if [[ ! ${DESTINATION} =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
                            echo "$(tput setaf 1) A-record type was specified and --include-reverse flag submitted, but it seems value is not an IP-address or adresses.  $(tput sgr0)" >&2
                            exit 1
                        fi
                        PTRADDR=$(echo ${DESTINATION} | sed -re 's:([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+):\4.\3.\2.\1.in-addr.arpa.:')
                        NSUPDATE_COMMAND+="\nupdate delete ${PTRADDR} ptr\nupdate add ${PTRADDR} ${TTL} PTR ${SOURCE}\nsend\nquit"
                    else
                        # Finishing A-record manipulations here
                        NSUPDATE_COMMAND+="\nquit"
                    fi
                fi

                # We assume a single value per record is done for other record types
                if [[ ${TYPE} =~ ^CNAME|TXT|SRV$ ]]; then

                    RESULT=$(dig +nocookie +short -t ${TYPE} @${DNS_AD_SERVER} ${SOURCE})
                    
                    if [[ ! -z ${RESULT} && -z ${FORCE_REQUESTED} ]]; then
                        echo "An existing record found (${RESULT}), but no --force flag is supplied. Exiting." >&2 
                        exit 1
                    fi

                    # If no record found at all, we simply add it
                    if [[ -z ${RESULT} ]]; then
                        echo "Existing record ${SOURCE} wasnt found, creating new."
                        NSUPDATE_COMMAND="server ${DNS_AD_SERVER}\nupdate add ${SOURCE} ${TTL} ${TYPE} ${DESTINATION}\nsend\nquit"
                    fi

                    # If record is found and --force flag is specified, we forcefully delete the existing one (even if it has multiple IP-addresses mapped)
                    if [[ ! -z ${RESULT} && ! -z ${FORCE_REQUESTED} ]]; then
                        echo "An existing record found (${RESULT} and --force flag is specified, the record will be removed COMPLETELY!"
                        NSUPDATE_COMMAND="server ${DNS_AD_SERVER}\nupdate delete ${SOURCE} ${TYPE}\nsend\nupdate add ${SOURCE} ${TTL} ${TYPE} ${DESTINATION}\nsend\nquit"
                    fi

                fi

                # Do the magic already
                if [[ ${USE_KEYTAB} == "yes" ]]; then
                    get_tgt ${KEYTAB} ${PRINCIPAL} || { exit 1; }
                fi
                { echo -e "${NSUPDATE_COMMAND}" | ${NSUPDATE} && echo "Successfully updated DNS-record ${SOURCE}"; } || { exit 1; }

            ;;

            delete)

                # A special case for A records if we want to delete PTR as well. Also, this works for A-records with single values only!
                if [[ ${TYPE} =~ ^A$ ]]; then

                    if [[ ! -z ${PTR_REQUESTED} ]]; then
                        PTRADDR=$(echo ${DESTINATION} | sed -re 's:([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+):\4.\3.\2.\1.in-addr.arpa.:')
                        NSUPDATE_COMMAND+="server ${DNS_AD_SERVER}\nupdate delete ${SOURCE} ${TYPE} ${DESTINATION}\nsend\nupdate delete ${PTRADDR} ptr\nsend\nquit"
                    else
                        NSUPDATE_COMMAND="server ${DNS_AD_SERVER}\n"
                        if [[ ! -z ${DESTINATION} ]]; then
                            MULTIRECORD_HOSTS=( $(echo "${DESTINATION}" | tr ',' ' ' ) )
                            for host in ${MULTIRECORD_HOSTS[*]}; do 
                                NSUPDATE_COMMAND+="update delete ${SOURCE} ${TYPE} ${host}\n"
                            done
                            NSUPDATE_COMMAND+="send\nquit"
                        else
                            NSUPDATE_COMMAND+="update delete ${SOURCE} ${TYPE}\nsend\nquit"
                        fi
                    fi

                    RESULT=$(dig +nocookie +short -t A @${DNS_AD_SERVER} ${SOURCE})

                fi

                # There may be cases when we want to delete PTRs only
                if [[ ${TYPE} =~ PTR ]]; then
                    # Fail-safe check to make sure user hasn't submitted illegal value
                    if [[ ! ${DESTINATION} =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
                        { echo "$(tput setaf 1) A-record type was specified and --update-reverse flag submitted, but it seems that value is not an IP-address or not a single IP-address is specified.  $(tput sgr0)" >&2; exit 1; }
                    fi
                    PTRADDR=$(echo ${SOURCE} | sed -re 's:([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+):\4.\3.\2.\1.in-addr.arpa:')
                    NSUPDATE_COMMAND="server ${DNS_AD_SERVER}\nupdate delete ${PTRADDR} ptr\nsend\nquit"

                    RESULT=$(dig +nocookie +short -t PTR @${DNS_AD_SERVER} ${PTRADDR})
                fi

                # Check that DNS-record exists at all
                if [[ ${TYPE} =~ CNAME|TXT|SRV ]]; then
                    NSUPDATE_COMMAND="server ${DNS_AD_SERVER}\nupdate delete ${SOURCE} ${TYPE} ${DESTINATION}\nsend\nquit"

                    RESULT=$(dig +nocookie +short -t ${TYPE} @${DNS_AD_SERVER} ${SOURCE})
                fi

                # We don't disturb DNS-servers if record is missing already
                if [[ -z ${RESULT} ]]; then
                    echo "The record is already missing. Doing nothing."
                    exit 0
                else
                    if [[ ${USE_KEYTAB} == "yes" ]]; then
                        get_tgt ${KEYTAB} ${PRINCIPAL} || { exit 1; }
                    fi
                    { echo -e "${NSUPDATE_COMMAND}" | ${NSUPDATE} && echo "Successfully deleted DNS-record ${SOURCE}"; } || { exit 1; }
                fi
            ;;

            *) echo "-a is supposed to be \"add\" or \"delete\"" >&2 ; { exit 1; }
            ;;
esac
