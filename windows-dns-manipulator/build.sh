#!/bin/bash

image_name="windows-dns-manipulator"
listen_port="50000"
container_port="80"

echo ">>> Removing old containers..."
old_container_id=$(docker ps -q -f ancestor=${image_name})
old_docker_image=$(docker images -f reference=${image_name} --format '{{.ID}}')
docker stop $old_container_id || true
docker rm $old_container_id || true

echo ">>> Removing old images..."
docker rmi $old_docker_image || true

echo ">>> Building new Docker Image..."
docker build --build-arg KERBEROS_KEYTAB=$SYSOPS_DNS_OPERATOR_KEYTAB \
             --tag ${image_name} \
             --file ./${image_name}/Dockerfile ./${image_name}

echo ">>> Starting $image_name container on local port TCP ${listen_port}..."
docker run -p 127.0.0.1:${listen_port}:${container_port} \
		   --detach \
		   -e FRNTD_AUTH_HEADER=$FRNTD_AUTH_HEADER \
		   -e FRNTD_AUTH_HEADER_SECRET=$FRNTD_AUTH_HEADER_SECRET \
		   -e FRNTD_POD_BUILDOUT_AUTH_HEADER=$FRNTD_POD_BUILDOUT_AUTH_HEADER \
		   -e FRNTD_POD_BUILDOUT_AUTH_HEADER_SECRET=$FRNTD_POD_BUILDOUT_AUTH_HEADER_SECRET \
		   -e FRNTD_POD_BUILDOUT_AUTH_START_TIME=$FRNTD_POD_BUILDOUT_AUTH_START_TIME \
		   -e FRNTD_POD_BUILDOUT_AUTH_END_TIME=$FRNTD_POD_BUILDOUT_AUTH_END_TIME \
           ${image_name}

echo ">>> Checking that container is still running..."
sleep 5
test -n "$(docker ps -q -f ancestor=$image_name)" && echo "Done"