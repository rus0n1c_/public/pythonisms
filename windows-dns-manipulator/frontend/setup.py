from setuptools import setup, find_packages

setup(
    name="windows-dns-manipulator-frontend",
    version="1.0",
    packages=find_packages(),
    include_package_data=True,

    install_requires=['flask>=1.1.1',
                      'requests>=2.9.1',
                      'jsonschema'
                      ],

    scripts=['frntd'],

    data_files=[
        ('/windows-dns-manipulator/templates', ['templates/index.html']),
        ('/windows-dns-manipulator/templates', ['templates/dns-request-json-schema.json'])
    ],

    license='MIT',
    author="Alexey Gagarin",
    author_email="public@incodewetrust.net",
    description="Frontend for Windows DNS Manipulation web-server",
    project_urls={
        "Source Code": "https://bitbucket.org/rus0n1c/pythonisms/windows-dns-manipulator",
    }
)
