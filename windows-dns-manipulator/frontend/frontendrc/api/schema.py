import json
import os


from frontendrc.util.shared_variables import FRNTD_SCHEMA_FILE
from frontendrc.tools.json_response import return_jsoned_response
from flask import request, make_response, Blueprint

return_schema = Blueprint('schema', __name__)

@return_schema.route('/api/schema', methods=['GET'])
def return_current_schema():
    """
    Return current Json Schema file used for validation
    """

    if not os.path.isfile(FRNTD_SCHEMA_FILE):
        
        response = return_jsoned_response("404 Not Found",
                                          "DNS JSON Schema File not found at %s" % FRNTD_SCHEMA_FILE,
                                          "Check that FRNTD_SCHEMA_FILE environment variable points to correct schema file", 404)

        response.headers['Content-Type'] = 'application/json'
        return response

    if not os.stat(FRNTD_SCHEMA_FILE).st_size:

        response = return_jsoned_response("422 Unprocessible Entity",
                                          "DNS JSON Schema File is found at %s, but appears to be empty" % FRNTD_SCHEMA_FILE,
                                          "Check that FRNTD_SCHEMA_FILE environment variable points to correct schema file", 422)
        response.headers['Content-Type'] = 'application/json'
        return response

    with open(FRNTD_SCHEMA_FILE) as schema_file:
        schema_content = schema_file.read()

    json_data = json.loads(schema_content)
    return json_data