

import os
import json


from frontendrc.tools.json_response import return_jsoned_response

from flask import request, make_response, Blueprint

version = Blueprint('version', __name__)

@version.route('/api/version', methods=['GET'])
def validate_incoming_json():
    """
    Return current version and documentation/project links
    """

    __current_version__ = (1, 0, 0)
    version = '.'.join(str(c) for c in __current_version__)

    return_data_raw = {'Name': 'Windows DNS Manipulator',
                       'Version': version,
                       'Description': 'Change DNS-records on Windows servers via Kerberos GSS-TSIG',
                       'GIT URL': 'https://pos/bitbucket.org/rus0n1c/pos/windows-dns-manipulator'}

    return_data_json = json.dumps(return_data_raw)

    return return_data_json