from __future__ import absolute_import

import os
import json
from frontendrc.util.exceptions import (manipulator_json_invalid,
                                        manipulator_json_schema_mismatch)

from frontendrc.tools.json_validator import is_json_valid
from frontendrc.tools.json_response import return_jsoned_response
from frontendrc.util.shared_variables import FRNTD_SCHEMA_FILE
from flask import request, make_response, Blueprint



validator = Blueprint('validate', __name__)

@validator.route('/api/validate', methods=['POST'])
def validate_incoming_json():
    """
    Check via is_json_valid function if the incoming json complies with JSON schema
    """

    if not request.headers.get('Content-Type'):
        response = make_response(return_jsoned_response("400 Bad Request",
                                                        "No \'Content-Type\' header is found in request",
                                                        "An \'application/json'\' is expected in request"), 400)
        response.headers['Content-Type'] = 'application/json'
        return response

    if request.headers.get('Content-Type') and request.headers.get('Content-Type') != 'application/json':
        response = make_response(return_jsoned_response("400 Bad Request",
                                                        "\'Content-Type\' header is found but it is not \'application/json\'",
                                                        "Check your client's HTTP headers"), 400)
        response.headers['Content-Type'] = 'application/json'
        return response


    if not os.path.isfile(FRNTD_SCHEMA_FILE):

        response = make_response(return_jsoned_response("404 Not Found",
                                                        "DNS JSON Schema File not found at %s" % FRNTD_SCHEMA_FILE,
                                                        "Check that FRNTD_SCHEMA_FILE environment variable points to correct schema file"), 404)
        response.headers['Content-Type'] = 'application/json'
        return response

    if not os.stat(FRNTD_SCHEMA_FILE).st_size:
        response = make_response(return_jsoned_response("422 Unprocessible Entity",
                                                        "DNS JSON Schema File is found at %s, but appears to be empty" % FRNTD_SCHEMA_FILE,
                                                        "Check that FRNTD_SCHEMA_FILE environment variable points to correct schema file"), 422)
        response.headers['Content-Type'] = 'application/json'
        return response

    try:
        with open(FRNTD_SCHEMA_FILE, 'r') as schema_file:
            schema_content = schema_file.read()
        json.loads(schema_content)
    except json.decoder.JSONDecodeError as e:
        response = make_response(return_jsoned_response("422 Unprocessible Entity",
                                                        "DNS JSON Schema File is found at %s, but appears to be malformed or not JSON at all" % FRNTD_SCHEMA_FILE,
                                                        "Check that FRNTD_SCHEMA_FILE is a valid JSON file"), 422)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Now check the client's input
    try:
        data = request.json
        validation_result = is_json_valid(schema_content, data)

        if not validation_result:

            response = make_response(return_jsoned_response("200 OK",
                                                            "JSON is valid and complies with JSON DNS schema",
                                                            "You can upload your JSON to /api/submit to change DNS-records"), 200)
            response.headers['Content-Type'] = 'application/json'
            return response
             

    except manipulator_json_invalid as e:
        response = make_response(return_jsoned_response("400 Bad Request",
                                                        "User-submitted DNS JSON is malformed or not JSON at all",
                                                        str(e)), 400)
        response.headers['Content-Type'] = 'application/json'
        return response


    except manipulator_json_schema_mismatch as e:
        response = make_response(return_jsoned_response("400 Bad Request",
                                                        "User-submitted DNS JSON does not comply with defined JSON Schema",
                                                        e.message), 400)
        response.headers['Content-Type'] = 'application/json'
        return response