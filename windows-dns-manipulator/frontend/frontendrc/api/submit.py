# from __future__ import absolute_import

# from bottle import (request,
#                     response,
#                     request,
#                     post)

# from dashbuilderrc.util.isBase64 import isBase64
# from dashbuilderrc.util.exceptions import (DBLDInvalidJinjaTemplate,
#                                            DBLDUnrecognizedJSON,
#                                            DBLDProposedVersionIsEqualToCurrent,
#                                            DBLDProposedVersionIsLowerThanCurrent,
#                                            DBLDInvalidDashboardVersionFormat)

# from dashbuilderrc.tools.jsonGrafanaGenerator import generateGrafanaJSON
# from dashbuilderrc.tools.jsonDashboardVersionChecker import checkDashboardVersion
# from dashbuilderrc.tools.jsonResponse import returnJsonResponse


from __future__ import absolute_import
from flask import request, make_response, Blueprint

from frontendrc.tools.json_response import return_jsoned_response
from frontendrc.tools.network_checks import *
from frontendrc.api.validate import validate_incoming_json
from frontendrc.util.shared_variables import (FRNTD_AUTH_HEADER,
                                              FRNTD_AUTH_HEADER_SECRET,
                                              FRNTD_POD_BUILDOUT_AUTH_HEADER,
                                              FRNTD_POD_BUILDOUT_AUTH_HEADER_SECRET,
                                              FRNTD_POD_BUILDOUT_AUTH_START_TIME,
                                              FRNTD_POD_BUILDOUT_AUTH_END_TIME)

import subprocess
import json
import os
import datetime

dns_request = Blueprint('submit', __name__)

@dns_request.route('/api/submit', methods=['POST'])
def submit_to_server():
    """
    Submit request for DNS change to Windows DNS-server
    """
 
    if (not request.headers.get(FRNTD_POD_BUILDOUT_AUTH_HEADER)) and (not request.headers.get(FRNTD_AUTH_HEADER)):

        response = make_response(return_jsoned_response("401 Unauthorized",
                                                        "No auth header is found in request, request denied.",
                                                        "Please, contact SysOPS Infra team for details"), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    if request.headers.get(FRNTD_AUTH_HEADER) and request.headers.get(FRNTD_AUTH_HEADER) != FRNTD_AUTH_HEADER_SECRET:
        response = make_response(return_jsoned_response("401 Unauthorized",
                                                        "Auth header is found in request, but value doesn't match expected secret, request denied.",
                                                        "Please, contact SysOPS team for details"), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # OPS-185953 start
    if (request.headers.get(FRNTD_POD_BUILDOUT_AUTH_HEADER)) and (request.headers.get(FRNTD_POD_BUILDOUT_AUTH_HEADER) != FRNTD_POD_BUILDOUT_AUTH_HEADER_SECRET):
        response = make_response(return_jsoned_response("401 Unauthorized",
                                                        "POD Buildout dedicated auth header is found in request, but value doesn't match expected secret, request denied.",
                                                        "Please, contact SysOPS Infra team for details"), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    if (request.headers.get(FRNTD_POD_BUILDOUT_AUTH_HEADER)) and (request.headers.get(FRNTD_POD_BUILDOUT_AUTH_HEADER) == FRNTD_POD_BUILDOUT_AUTH_HEADER_SECRET):
        # Verify that specified time frame allows to perform DNS changes for POD Buildout header
        pod_buildout_start_time = datetime.datetime.strptime(FRNTD_POD_BUILDOUT_AUTH_START_TIME, '%d.%m.%Y')
        pod_buildout_end_time   = datetime.datetime.strptime(FRNTD_POD_BUILDOUT_AUTH_END_TIME, '%d.%m.%Y')

        # Check that allowed time has already started or end time has not yet been reached
        delta_start = pod_buildout_start_time - datetime.datetime.now()
        delta_end   = pod_buildout_end_time - datetime.datetime.now()
        if delta_start.total_seconds() >= 0:
            response = make_response(return_jsoned_response("423 Locked",
                                                            "POD Buildout dedicated auth header/secret were accepted, but allowed time range has not yet started, request denied.",
                                                            "Please, ask SysOPS Infra team to adjust time range period"), 401)
            response.headers['Content-Type'] = 'application/json'
            return response

        if delta_end.total_seconds() <= 0:
            response = make_response(return_jsoned_response("423 Locked",
                                                            "POD Buildout dedicated auth header/secret were accepted, but allowed time range has already been passed, request denied.",
                                                            "Please, ask SysOPS Infra team to adjust time range period"), 401)
            response.headers['Content-Type'] = 'application/json'
            return response

    user_input_validation_result = validate_incoming_json()
    
    if user_input_validation_result.status != "200 OK":
        return user_input_validation_result


    # Check that DNS-server's hostname resolves. We use getevent because standard gethostbyname doesn't allow to set timeout for DNS resolution
    dns_server = request.json['dns_record_dns_server']
    if not check_dns_resolution(dns_server):
        response = make_response(return_jsoned_response("424 Failed Dependency",
                                                        "Failed to resolve DNS-name %s" % dns_server,
                                                        "Verify the DNS-server you supplied is correct"), 424)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Check Kerberos TCP 88, DNS TCP 53
    for port in ['88', '53']:
        if not check_tcp_port(dns_server, port):
            response = make_response(return_jsoned_response("424 Failed Dependency",
                                                            "Server does not respond on port TCP %s" % port,
                                                            "Is is really Windows DNS server?"), 424)
            response.headers['Content-Type'] = 'application/json'
            return response




    # Finally, submit request to nsupdate-based script and get the result
    environment = os.environ.copy()
    environment["DNS_AD_SERVER"] = request.json['dns_record_dns_server']
    command_to_run = "dnsupdate -a %s -s %s -d %s -t %s -l %s %s %s" % (request.json['dns_record_action'],
                                                                         request.json['dns_record_source'],
                                                                         request.json['dns_record_value'],
                                                                         request.json['dns_record_type'],
                                                                         request.json['dns_record_ttl'] if 'dns_record_ttl' in request.json else "300",
                                                                         "-r" if 'dns_record_include_ptr' in request.json and request.json['dns_record_include_ptr'] == True else "",
                                                                         "-f" if request.json['dns_record_force_update'] == True else "")

    dns_request_result = subprocess.Popen(command_to_run, 
                                          shell=True,
                                          stdout=subprocess.PIPE,
                                          stderr=subprocess.PIPE,
                                          env=environment)

    stdout, stderr = dns_request_result.communicate()

    if dns_request_result.returncode != 0:
        response = make_response(return_jsoned_response("500 Internal Server Error",
                                                        "The underlying script exited with non-zero exit code",
                                                        str(stderr)), 500)
        response.headers['Content-Type'] = 'application/json'
        return response

    if dns_request_result.returncode == 0:
        response = make_response(return_jsoned_response("200 OK",
                                                        "The underlying script exited normally",
                                                        "The following command was executed: %s" % command_to_run), 200)
        response.headers['Content-Type'] = 'application/json'
        return response
