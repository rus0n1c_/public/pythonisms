
class manipulator_operational_exception(Exception):
    """
    Main Windows DNS Manipulator Exception Wrapper
    """
    def __init__(self, message):
        self.message = message
        super().__init__(message)


class manipulator_json_invalid(manipulator_operational_exception):
    """
    Raise if JSON is malformed
    """ 
    pass

class manipulator_json_schema_mismatch(manipulator_operational_exception):
    """
    Raise if JSON doesn't comply with schema
    """
    pass

class manipulator_dns_server_unresponsive(manipulator_operational_exception):
    """
    Raise if Windows DNS server is unresponsive on 53
    """