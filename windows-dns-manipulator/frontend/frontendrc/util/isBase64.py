import base64


def isBase64(s, decode=False):

    try:
        base64.b64encode(base64.b64decode(s)) == s
        if not decode:
            return True
        if decode:
            decoded = base64.b64decode(s)
            return decoded.decode('utf-8').strip('\n')
    except Exception:
        return False
