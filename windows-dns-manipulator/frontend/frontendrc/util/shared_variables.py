# -- Global configuration file -- #

import os

# -- We use predefined variables if no environmental vars are supplied -- #

FRNTD_LISTEN_ADDRESS     = os.environ['FRNTD_LISTEN_ADDRESS'] if 'FRNTD_LISTEN_ADDRESS' in os.environ else '0.0.0.0'
FRNTD_LISTEN_PORT        = os.environ['FRNTD_LISTEN_PORT'] if 'FRNTD_LISTEN_PORT' in os.environ else '80'
FRNTD_INDEX_FILE         = os.environ['FRNTD_INDEX_FILE'] if 'FRNTD_INDEX_FILE' in os.environ else '/windows-dns-manipulator/templates/index.html'
FRNTD_SCHEMA_FILE        = os.environ['FRNTD_SCHEMA_FILE'] if 'FRNTD_SCHEMA_FILE' in os.environ else '/windows-dns-manipulator/templates/dns-request-json-schema.json'

# This is a temporary solution to somehow protect the web-server from completely unsupervised access. MUST BE redesigned later into something better
FRNTD_AUTH_HEADER        = os.environ['FRNTD_AUTH_HEADER']
FRNTD_AUTH_HEADER_SECRET = os.environ['FRNTD_AUTH_HEADER_SECRET']

# OPS-185953
FRNTD_POD_BUILDOUT_AUTH_HEADER        = os.environ['FRNTD_POD_BUILDOUT_AUTH_HEADER']
FRNTD_POD_BUILDOUT_AUTH_HEADER_SECRET = os.environ['FRNTD_POD_BUILDOUT_AUTH_HEADER_SECRET']
FRNTD_POD_BUILDOUT_AUTH_START_TIME    = os.environ['FRNTD_POD_BUILDOUT_AUTH_START_TIME']
FRNTD_POD_BUILDOUT_AUTH_END_TIME      = os.environ['FRNTD_POD_BUILDOUT_AUTH_END_TIME']