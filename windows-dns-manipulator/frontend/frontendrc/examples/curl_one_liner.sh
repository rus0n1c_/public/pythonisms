#!/bin/bash

. /lib/lsb/init-functions

TTL=${5:-"60"}
PTR=${6:-false}

EXPECTED_ENV_VARS=( DNS_SERVER DNS_API_URL DNS_API_HEADER DNS_API_SECRET )
for ENV_VAR in ${EXPECTED_ENV_VARS[*]}; do
	if [[ -z ${!ENV_VAR} ]]; then	
		log_failure_msg "Checking ${ENV_VAR} is set..."
		exit 1
	else
		log_success_msg "Checking ${ENV_VAR} is set..."
	fi
done

if [[ -z "$1" || -z "$2" || -z "$3" || -z "$4" ]]; then
cat <<EOF
Not all input is supplied, can't proceed.

Examples of usage:

1. Add A-record "rocketman.ops.contoso.com with values 1.1.1.1,2.2.2.2 
$(basename $BASH_SOURCE) add a rocketman.ops.contoso.com 1.1.1.1,2.2.2.2

2. Delete CNAME i-am-a-cname.contoso.com pointing to yandex.ru
$(basename $BASH_SOURCE) delete cname i-am-a-cname.contoso.com yandex.ru

3. Add A-record i-am-a-ptr.contoso.com pointing at 1.2.3.4 along with PTR-record and TTL 3600
$(basename $BASH_SOURCE) add a i-am-a-ptr.contoso.com 1.2.3.4 3600 true

EOF
exit 1
fi


#RES=$(curl -s -o /dev/null -w "%{http_code}" -X POST -H "${DNS_API_HEADER}: ${DNS_API_SECRET}" -H "Content-Type: application/json" \
RES=$(curl --http1.1 -X POST -H "${DNS_API_HEADER}: ${DNS_API_SECRET}" -H "Content-Type: application/json" \
-d '{"dns_record_type": "'$2'", "dns_record_action": "'$1'", "dns_record_source": "'$3'", "dns_record_value": "'$4'", "dns_record_dns_server": "'${DNS_SERVER}'", "dns_record_force_update": true, "dns_record_ttl": "'${TTL}'", "dns_record_include_ptr": '${PTR}' }' \
${DNS_API_URL}/api/submit)

if [[ "${RES}" == "200" ]]; then
	log_success_msg "Result of DNS request: ${RES}"
else
	log_failure_msg "Result of DNS request: ${RES}"
fi
