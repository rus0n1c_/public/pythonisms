import subprocess
import socket


def check_dns_resolution(fqdn, timeout=1):
    """
    Checks if DNS-name is resolved. Relies on system resolver.
    Note that we run "host" to check if DNS-name is resolvable.

    !!! This is a cheap and fast solution not to wrap the spaghetti-code around gethostbyname because it doesn't provide timeout natively !!!

    If you know a better solution, please, do not hesitate to modify this function.

    """


    command_to_run = "host -W %s %s" % (timeout, fqdn)
    is_fqdn_resolvable = subprocess.Popen(command_to_run, 
                                          shell=True,
                                          stdout=subprocess.PIPE,
                                          stderr=subprocess.PIPE)

    stdout, stderr = is_fqdn_resolvable.communicate()
    if is_fqdn_resolvable.returncode != 0:
        return False
    if is_fqdn_resolvable.returncode == 0:
        return True

def check_tcp_port(fqdn, port, timeout=2):
    """
    Check TCP port responds
    """

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(timeout)
    try:
        s.connect( (fqdn, int(port)) )
        s.shutdown(socket.SHUT_RDWR)
        return True
    except:
        return False
    finally:
        s.close()