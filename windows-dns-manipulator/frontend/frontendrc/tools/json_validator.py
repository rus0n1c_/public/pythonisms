from frontendrc.util.exceptions import manipulator_json_invalid, manipulator_json_schema_mismatch

import jsonschema
import json

def is_json_valid(json_schema, json_data):
    """
    Check that 'json_data' is valid and complies with 'json_schema'
    """
    try:
        jdata = json.loads(json.dumps(json_data))
        jschema = json.loads(json_schema)
    except json.decoder.JSONDecodeError as e:
        raise manipulator_json_invalid( str(e) )

    try:
        result = jsonschema.validate(instance=jdata, schema=jschema)
        if not result:
            return None
    except (jsonschema.exceptions.ValidationError, jsonschema.exceptions.FormatError) as e:
        raise manipulator_json_schema_mismatch(e.message)