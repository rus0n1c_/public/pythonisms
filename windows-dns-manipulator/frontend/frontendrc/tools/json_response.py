import json

def return_jsoned_response(http_code, reason, details=None):
    """
    Just small function to get rid of repetitive code in main project
    """

    return json.dumps({"HTTP Code": http_code,
                       "Reason": reason,
                       "Details": details})