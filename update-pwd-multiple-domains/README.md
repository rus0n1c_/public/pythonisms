Please, install https://bitbucket.org/pythonisms/python-connectors before installing this script!

Also, Python 3.6 and higher must be used!

Update AD password for multiple domains at once. Large companies may have several AD-domains to work at, it is quite cumbersome to spend time updating password at 4+ domains by hand. 

Expects dict of domains (see domain.yml as example)

Example: adpwd -u john.snow -c domains.yml -op '<some_old_pwd>' -np '<some_new_pwd>'
