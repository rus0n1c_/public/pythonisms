from setuptools import setup, find_packages

setup(
    name="update_domain_pwd",
    version="1.0",
    packages=find_packages(),
    include_package_data=True,

    install_requires=['ldap3>=2.5'],

    scripts=['adpwd'],

    data_files=[
    ],

    license='MIT',
    author="Alexey Gagarin",
    author_email="agagarin@incodewetrust.net",
    description="Update Active Directory password for multiple domains at once",
    project_urls={
        "Source Code": "https://bitbucket.org/pythonisms"
    }
)
