from setuptools import setup, find_packages

setup(
    name="adgroup",
    version="1.0",
    packages=find_packages(),
    include_package_data=True,

    install_requires=['ldap3>=2.5',
                      'python-connectors'],

    scripts=['adgroup'],

    data_files=[
    ],

    license='MIT',
    author="Alexey Gagarin",
    author_email="agagarin@incodewtrust.net",
    description="Add/Delete user from Active Directory group",
    project_urls={
        "Source Code": "https://bitbucket.org/rus0n1c/pythonisms/add_delete_from_ad_group"
    }
)
