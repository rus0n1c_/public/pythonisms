Please, install [python-connectors](https://gitlab.com/rus0n1c_/public/pythonisms/-/tree/main/python-connectors) before installing this script!

:warning: Python 3.6 and higher is expected.

This script expects ```AD_USERNAME```, ```AD_PASSWORD``` and ```AD_SERVER``` environmental variables to be set.

Example:
```bash
export AD_USERNAME="john.snow@somedomain.com"
export AD_SERVER="dc1.somedomain.com"
export AD_PASSWORD="somepass"

# Add jane.dow to Some_Group_Name
adgroup -u jane.dow -a add -g "Some_Group_Name"

# Delete jane.dow from Some_Group_Name
adgroup -u jane.dow -a delete -g "Some_Group_Name"
```
