# -*- coding: utf-8 -*-

import requests
from requests.exceptions import ConnectionError
import json
import time

class cmdbConnector():
	"""
	ObjectSchemaID - Insigth CMDB schema ID.
	"""
	def __init__(self, login, password, URL, objectSchemaID='1'):
		self.login = login
		self.password = password
		self.URL = u''.join(URL).encode('utf-8')
		self.objectSchemaID = objectSchemaID

	def initSession(self):
		
		self.cmdbSession = requests.Session()

		try:
			self.cmdbConn = self.cmdbSession.get(self.URL)
		
			time.sleep(2)

		# -- Browser-like headers for initial login -- 
		
			self.payload = {
							'os_username' : self.login,
							'os_password' : self.password,
							'os_destination' : '',
							'user_role' : '',
							'atl_token' : '',
							'login' : 'Вход'
							}
		
			self.headers = {
							'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36',
							'Accept-Language' : 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
							'Accept-Encoding' : 'gzip,deflate,sdch',
							'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
							}		
					
			self.cmdbConn = self.cmdbSession.post(self.URL, data=self.payload,headers=self.headers, allow_redirects=True)
			if self.cmdbConn.status_code == 401:
				raise RuntimeError('Connection to JIRA CMDB failed: Check your login or password')

		except ConnectionError as e:
			raise RuntimeError('Connection to JIRA CMDB failed : Check your URL')
			

	def cmdbListObjects(self,cmdbURL, ObjectTypeID, returnNamesOnly=False, objectSchemaID=None, resultsPerPage='1000'):
		"""
		Gets all CMDB-objects of supplied objectTypeID and all related attributes. Loads off in JSON.
		
		objectTypeID - Uniqie identifier of CMDB-object, i.e. 333 for switches, 345 for routers etc. 
		returnNamesOnly - returns list of with 'Name' attribute of all CMDB-objects with supplied objectTypeID if set to True, else returns nested JSON with all objects and all defined attributes.
		"""
		
		if objectSchemaID is None:
			objectSchemaID = self.objectSchemaID
		
		cmdbURL = u''.join(cmdbURL).encode('utf-8')
		iql = "%s/rest/insight/1.0/iql/objects?objectSchemaId=%s&iql=ObjectTypeId=%s&resultPerPage=%s" % (cmdbURL,objectSchemaID,ObjectTypeID,resultsPerPage)
		self.objectListRaw = self.cmdbSession.get(iql)
		self.objectListRaw = json.loads(self.objectListRaw.text)
		if not self.objectListRaw['objectEntries']:
			raise RuntimeError('Empty objectEntries. Is ObjectID valid?')
		else:
			if not returnNamesOnly:
				return self.objectListRaw['objectEntries']
			elif returnNamesOnly:
				return [obj['name'] for obj in self.objectListRaw['objectEntries']]
			
	def cmdbGetObjectByName(self, cmdbURL,cmdbObjectName,objectSchemaID=None):
		"""
		Similar to cmdbListObjects, but only for single CMDB-object. Needs 'Name' attribute to be supplied. Returns JSON.
		"""

		if objectSchemaID is None:
			objectSchemaID = self.objectSchemaID
		
		cmdbURL = u''.join(cmdbURL).encode('utf-8')
		self.cmdbObjectName = u''.join(cmdbObjectName).encode('utf-8')
	
		iql = "%s/rest/insight/1.0/iql/objects?objectSchemaId=%s&iql=Name=%s" % (cmdbURL,objectSchemaID,self.cmdbObjectName)
		self.rawInfo = self.cmdbSession.get(iql)
		self.jsonInfo = json.loads(self.rawInfo.text)

		return self.jsonInfo

	def cmdbGetObjectKey(self, cmdbURL, cmdbObjectName, objectSchemaID=None):
		"""
		Returns CMDB Object Key ('CMDB-1234') for supplied 'Name' of CMDB object. Useful when one needs to update some attributes.
		"""
		
		raw = self.cmdbGetObjectByName(cmdbURL, cmdbObjectName, objectSchemaID)
		if raw.has_key('objectEntries') and raw['objectEntries']:
			return raw['objectEntries'][0]['objectKey']
		else:
			raise RuntimeError('Empty objectEntries. Does item really exist?')
			
	
	def cmdbCompileObjectList(self,cmdbURL,ObjectList,objectSchemaID=None):
		"""
		Returns list of CMDB-objects and theis respective attributes in JSON. Differs from cmdbListObjects fucntion on terms of creating JSON for supplied objects with 'Name' attribute in ObjectList variable, no matter what the ObjectTypeID is.
		cmdbListObjects returns list for all objects of supplied ObjectTypeID.
		
		Функция для формирования списка объектов с их атрибутами в виде вложенного JSON. Отличие от функции cmdbListObjects в том, что список с JSON-атрибутами формируется только для нескольких, явно указанных по имени, объектов 
		(переменная ObjectList), независимо от их ObjectTypeID.  В cmdbListObjects список формируется независимо от имени и только для объектов, чей ObjectTypeID передан в функцию.
		ObjectList - список объектов в CMDB (поле Name).
		"""
		
		if objectSchemaID is None:
			objectSchemaID = self.objectSchemaID
		
		cmdbURL = u''.join(cmdbURL).encode('utf-8')
		self.ObjectList = ObjectList
		self.cmdbCompiledInfo = {}
		
		for cmdbObject in self.ObjectList:
			result = self.cmdbGetObjectByName(cmdbURL,cmdbObject,objectSchemaID	)
			self.cmdbCompiledInfo[cmdbObject] = result
	
	def cmdbUpdateObjectAttributes(self, cmdbURL, ObjectID, attributesToUpdate):
		"""	
		Updates attributes of single CMDB object.
		ObjectID - self-explanatory.
		attributesToUpdate - dict of attributes to update. Expected format: {<attribute ID 1> : <ID 1 value>, <attribute ID 2> : <ID 2 value>, ... , <attribute ID N>: <ID N value>}. Example: <{'3232' : 'CentralRouter'}
		
		Функция изменения атрибутов единичного объекта в CMDB.
		ObjectID - ID объекта.
		attributesToUpdate - словарь с атрибутами, которые надо передать в CMDB. Должен быть в формате {<ID атрибута1> : <значение атрибута1>,<ID атрибута12> : <значение атрибута2>...,<ID атрибутаN> : <значение атрибутаN>}, например, {'1234' : 'sw-i-acs-01'} 
		"""			
		
		self.ObjectID = ObjectID if isinstance(ObjectID, basestring) else str(ObjectID)
		
		self.attributesToUpdate = attributesToUpdate
		if not isinstance(self.attributesToUpdate,dict):
			raise RuntimeError('Attributes not in dict format')
		
		self.cmdbJSON = {'attributes': []}
		
		# -- Compiling JSON -- #
		
		for cursor in self.attributesToUpdate:
			self.cmdbJSON['attributes'].append({'objectTypeAttributeId' : cursor, 'objectAttributeValues': [{'value' : self.attributesToUpdate[cursor]}]})
		self.cmdbJSON = json.dumps(self.cmdbJSON)
		
		# -- Updating attributes of CMDB object. Using PUT request. -- #
		
		jsonHeaders = self.cmdbSession.headers
		jsonHeaders['content-type'] = 'application/json'
		iql = "%s/rest/insight/1.0/object/%s" % (cmdbURL,self.ObjectID)
		self.update = self.cmdbSession.request("PUT", iql, data=self.cmdbJSON, headers=jsonHeaders)
		if self.update.status_code != 200:
			raise RuntimeError("Update of %s failed. JIRA has returned status code %s" % (self.ObjectID, self.update.status_code))
			
	def cmdbCreateObject(self,cmdbURL,ObjectTypeID,objectName,attributesToCreate):
		"""
		Creates object in CMDB.
		ObjectTypeID - object ID in CMDB.
		objectName - 'Name' attribute also has its own unique ID. We have to supply dictionary {<'ID of Name attribute'> : '<Name of new CMDB object>'}
		attributesToCreate - dict of attributes to create for new CMDB object. Expected format: {<attribute ID 1> : <ID 1 value>, <attribute ID 2> : <ID 2 value>, ... , <attribute ID N>: <ID N value>}. Example: <{'3232' : 'CentralRouter'}
		
		Функция создания объекта в CMDB.
		ObjectTypeID - ID объекта в CMDB, например, 333 для коммутаторов и т.д.
		objectName - у поля 'Name' в CMDB свой ObjectTypeAttributeID, поэтому словарь в виде {'<ID атрибута' : '<Имя объекта>' }.
		attributesToCreate - словарь с атрибутами, которые надо создать в CMDB. Должен быть в формате {<ID атрибута1> : <значение атрибута1>,<ID атрибута12> : <значение атрибута2>...,<ID атрибутаN> : <значение атрибутаN>}
		"""	
		
		self.attributesToCreate = attributesToCreate
		
		if not isinstance(self.attributesToCreate,dict):
			raise RuntimeError('Attributes not in dict format')

		if not isinstance(objectName,dict):
			raise RuntimeError('Virtual Machine Name is not dict Format')
		
		self.cmdbCreateJSON = {'attributes': [], 'objectTypeId' : str(ObjectTypeID)}
		
		self.cmdbCreateJSON['attributes'].append({'objectTypeAttributeId' : objectName.keys()[0], 'objectAttributeValues': [{'value' : objectName[objectName.keys()[0]]}]})
		
		for cursor in self.attributesToCreate:
			self.cmdbCreateJSON['attributes'].append({'objectTypeAttributeId' : cursor, 'objectAttributeValues': [{'value' : self.attributesToCreate[cursor]}]})
		self.cmdbCreateJSON = json.dumps(self.cmdbCreateJSON)
		
		jsonHeaders = self.cmdbSession.headers
		jsonHeaders['content-type'] = 'application/json'
		iql = "%s/rest/insight/1.0/object/create" %(cmdbURL)
		self.create = self.cmdbSession.request("POST", iql, data=self.cmdbCreateJSON, headers=jsonHeaders)
		
	def cmdbDeleteObject(self,cmdbURL,ObjectID):
		"""
		Deletes object from CMDB. Needs ObjectID.
		
		Функция удаления объекта по его ObjectID
		"""
		
		self.ObjectID = ObjectID if type(ObjectID) == str else str(ObjectID)
		iql = "%s/rest/insight/1.0/object/%s" % (cmdbURL, self.ObjectID)
		self.delete = self.cmdbSession.request("DELETE", iql)
		if self.delete.status_code != 200:
			raise RuntimeError("Deletion of %s failed. JIRA has returned status code %s" % (self.ObjectID, self.delete.status_code))

	def cmdbGetObjectAttributeValues(self,cmdbURL,objectName,textAttributesIDList,userAttributesIDList,refobjectAttributesIDList,statusAttributesIDList,includeObjectID=False,ObjectSchemaID='1',nameasReference=False):
		"""
		Returns dictionary of {<ID of attribute> : <value of attribute>} for single CMDB-object
		textAttributesIDList - attributes of type Default and subtype Text.
		userAttributesIDList - attributes of type User.
		refobjectAttributesIDList - attribute IDs referencing other CMDB-objects.
		statusAttributesIDList - attributes of type Status
		includeObjectID - flag. If set - toggles inclusion of ObjectID of CMDB-object.
		nameasReference - optional flag for refobjectAttributesIDList. If set to True, refobjectAttributesIDList is filled with internal ID of referenced object (i.e. CMDB-1234) and not 'Name'.
		
		Функция, возвращающая словарь в формате { <ID атрибута> : <значение атрибута> }
		textAttributesIDList - список из ID атрибутов типа Default с подтипом Text.
		userAttributesIDList - список из ID атрибутов типа User
		refobjectAttributesIDList - список из ID атрибутов, значение которых -- ссылка на другой объект в CMDB
		statusAttributesIDList - список из ID атрибутов типа Status 
		includeObjectID - флаг, который определяет, добавить ли в вывод результата ObjectID в обрабатываемого объекта.
		nameasReference - опциональный флаг для refobjectAttributesIDList. Если выставлен в True, то вместо значение в refobjectAttributesIDList записывается внутреннее имя объекта, на который ссылается значение (например, CMDB-1234).
		"""
		
		textAttributesIDList = [ int(id) for id in textAttributesIDList ]
		userAttributesIDList = [ int(id) for id in userAttributesIDList ]
		refobjectAttributesIDList = [ int(id) for id in refobjectAttributesIDList ]
		statusAttributesIDList = [ int(id) for id in statusAttributesIDList ]
		
		
		cmdbURL = u''.join(cmdbURL).encode('utf-8')
		objectName = u''.join(objectName).encode('utf-8')
		resultDict = {}
		
		if not textAttributesIDList and not userAttributesIDList and not refobjectAttributesIDList and not statusAttributesIDList:
			raise RuntimeError('All attribute IDs lists is empty.')
		if not isinstance(textAttributesIDList,list) or not isinstance(userAttributesIDList,list) or not isinstance(refobjectAttributesIDList,list) or not isinstance(statusAttributesIDList,list):
			raise RuntimeError('Attribute IDs not in list format')
		
		iql = "%s/rest/insight/1.0/iql/objects?objectSchemaId=%s&iql=Name=%s" % (cmdbURL, ObjectSchemaID, objectName)
		rawInfo = self.cmdbSession.get(iql)
		jsonInfo = json.loads(rawInfo.text)
		self.cmdbcmdb = jsonInfo
		
		if jsonInfo.has_key('errorMessages'):
			resultDict[objectName] = 'Error'
			return resultDict
		elif jsonInfo.has_key('objectEntries') and not jsonInfo['objectEntries']:
			resultDict[objectName] = ''
			return resultDict
		else:
			
			for attr in jsonInfo['objectEntries'][0]['attributes']:
				
				# -- ID <-> Value for textIDAttributesList fields -- #
				
				if attr['objectTypeAttributeId'] in textAttributesIDList:
					resultDict[attr['objectTypeAttributeId']] = attr['objectAttributeValues'][0]['value'] if attr['objectAttributeValues'] else ''
				
				# -- ID <-> Value for userIDAttributesList fields -- #
				
				elif attr['objectTypeAttributeId'] in userAttributesIDList:
					if attr['objectAttributeValues']:
						resultDict[attr['objectTypeAttributeId']] = [user['user']['name'] for user in attr['objectAttributeValues'] ]
					else:
						resultDict[attr['objectTypeAttributeId']] = ''
				
				# -- ID <-> Value for refobjectAttributesIDList fields -- #

				elif attr['objectTypeAttributeId'] in refobjectAttributesIDList:
					if not nameasReference:
						if attr['objectAttributeValues']:
							resultDict[attr['objectTypeAttributeId']] = [ obj['referencedObject']['name'] for obj in attr['objectAttributeValues'] ]
						else:
							resultDict[attr['objectTypeAttributeId']] = ''
					elif nameasReference:
						if attr['objectAttributeValues']:
							resultDict[attr['objectTypeAttributeId']] = [ obj['referencedObject']['objectKey'] for obj in attr['objectAttributeValues'] ]
						else:
							resultDict[attr['objectTypeAttributeId']] = ''						

				# -- ID <-> Value for statusAttributesIDList fields -- #
			
				elif attr['objectTypeAttributeId'] in statusAttributesIDList:
					if attr['objectAttributeValues']:
						resultDict[attr['objectTypeAttributeId']] = attr['objectAttributeValues'][0]['status']['id']
					else:
						resultDict[attr['objectTypeAttributeId']] = ''
			
			if includeObjectID:
				resultDict['ObjectID'] = jsonInfo['objectEntries'][0]['id']
				return resultDict
			elif not includeObjectID:
				return resultDict
			
