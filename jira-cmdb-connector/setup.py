from setuptools import setup

setup(name='jira-cmdb-connector',
	  version='1.0',
	  description='Jira Insight CMDB Plugin Connector',
	  url='https://bitbucket.org/rus0n1c/pythonisms/jira-cmdb-connector',
	  author='Alexey U.Gagarin',
	  author_email='bitbucket@incodewetrust.net',
	  license='MIT',
	  packages=['cmdbConnector'],
	  install_requires=['requests'],
	  zip_safe=False)
