#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import re
import sys
import easysnmp

def parserFunction(argv=None):

	parser = argparse.ArgumentParser(description="Returns switch port MAC-addresses as dictionary  {'Switch Name' : '<devicename', '<Switch Port>': {'VLAN': '<vlan>', 'MAC' :['MAC1','MAC2'...,'MACN']}}")
	parser.add_argument('--switchname',help='DNS-name or IP-address of switch',required=True)
	parser.add_argument('--snmpcommunity',help='Switch SNMP-community. v1 and v2c supported.', required=True)
	parsedParams = parser.parse_args(argv)
	return parsedParams

def collectMACAddressTable(device,snmpcommunity,location=''):
	"""
	Getting MAC-address table. OID 1.3.6.1.2.1.17.7.1.2.2.1.2;
	Получаем таблицу MAC-адресов с помощью OID 1.3.6.1.2.1.17.7.1.2.2.1.2; 
	"""
	
	def convertMacDecToHex(listofvalues):
		"""
		Converts decimal MAC-address OID to hex. Expects list of hex-parts of MAC.
		Маленькая функция для конвертирования десятичного представления OID с MAC-адресов в HEX. Ожидает список HEX-составляющих MAC-адреса в виде списка
		"""
		pluszero = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']
		mac = ''
		for hexmacpart in listofvalues:	
			converted = str(hex(int(hexmacpart))[2:]).upper()
			converted = '0'+converted if converted in pluszero else converted
			mac = mac + converted + '-'
		return mac[:-1]
		
	macTable = {}
	
	try:
		snmpwalk = easysnmp.Session(hostname=device,community=snmpcommunity,version=2,timeout=2,retries=1)
		raw = snmpwalk.walk('1.3.6.1.2.1.17.7.1.2.2.1.2')
	except (easysnmp.exceptions.EasySNMPTimeoutError, easysnmp.exceptions.EasySNMPError) as e:
		return e
	
	totalPortsOnSwitch = int(snmpwalk.get('.1.3.6.1.2.1.2.1.0').value)
	
	for port in xrange(0,totalPortsOnSwitch):
		macTable[port] = []
		for mac in raw:
			if int(mac.value) == port:
				tmp = re.sub('mib-2.17.7.1.2.2.1.2.','',mac.oid)
				tmp = tmp.split('.')
				macDict = {}
				macDict['VLAN'] = tmp[0]
				macDict['MAC'] = convertMacDecToHex(tmp[1:])
				macTable[port].append(macDict)
				macTable['SW'] = device
				macTable['Location'] = location

	for key,value in macTable.items():
		if len(value) < 1:
			macTable.pop(key)
	#print('Switch processed: ' + str(device))		
	return macTable

def findInterfacesByDescription(device,snmpcommunity,descriptionStrings):
	"""
	Retrieves ifDescr (OID .1.3.6.1.2.1.31.1.1.1.18) and returns interfaces with at least one string from descriptionStrings list.
	
	Функция, которая получает OID .1.3.6.1.2.1.31.1.1.1.18 (Описание интерфейсов) и возвращает список интерфейсов, у которых в описании есть хотя бы одна из строк списка descriptionStrings
	"""
		
	if type(descriptionStrings) != list:
		raise RuntimeError('Incorrect type of descriptionStrings variable. Should be list of strings.')
	else:
		try:
			snmpwalk = easysnmp.Session(hostname=device,community=snmpcommunity,version=2,timeout=2,retries=1)
			raw = snmpwalk.walk('1.3.6.1.2.1.31.1.1.1.18')
		except (easysnmp.exceptions.EasySNMPTimeoutError, easysnmp.exceptions.EasySNMPError) as e:
			return e
			
		portMatchList = [int(port.oid_index) for port in raw if port.value in descriptionStrings ]
									 
		return portMatchList
		
if __name__ == "__main__":
	
	parsedValues = parserFunction()
	
	try:
		collectMACAddressTable(parsedValues.switchname,parsedValues.snmpcommunity)
	except RuntimeError as e:
		print e
	
	
