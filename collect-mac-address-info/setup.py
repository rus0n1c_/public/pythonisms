from setuptools import setup

setup(name='collect-mac-address-info', version='0.1',description='MAC|VLAN Collection from every switch port',url='https://bitbucket.org/rus0n1c/collect-mac-address-info',author='Alexey U.Gagarin',author_email='bitbucket@incodewetrust.net',license='MIT',packages=['macCollect'],install_requires=['pymssql'],zip_safe=False)

print("-------------------------------------")
packages=['cmdbConnector']
for package in packages:
    try:
        __import__(package)
    except ImportError:
        print("Package %s is not installed. Please find it and git clone from https://bitbucket.org/rus0n1c/jira-cmdb-connector" % package)
        exit(1)
