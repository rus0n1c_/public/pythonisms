# -*- coding: utf-8 -*-

import sys,os,time

import ConfigParser
import logging
from cmdbConnector import cmdbConnector
from macCollect import collectMACAddressTable,findInterfacesByDescription
from mssqlConnector import mssqlConnector
from multiprocessing import Pool


config = ConfigParser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', 'Docs', 'config.cfg'))
logfile = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..','Logs', 'operation.log')
logging.basicConfig(filename=logfile,format='%(asctime)s -- %(levelname)s -- %(message)s',level=logging.INFO)

dbhost = eval(config.get("dbconnect","dbhost"))
dbname = eval(config.get("dbconnect","dbname"))
dbusername = eval(config.get("dbconnect","dbusername"))
dbpassword = eval(config.get("dbconnect","dbpassword"))

cmdblogin = eval(config.get("credentials","cmdbLogin"))
cmdbpassword = eval(config.get("credentials","cmdbPassword"))
cmdburl = eval(config.get("credentials","cmdbURL"))
cmdbObjectID = eval(config.get("cmdb","objectID"))
attribmapping = eval(config.get("cmdb","attribmapping"))
vendors = eval(config.get("cmdb","vendors"))

def collectActiveSW(cmdbLogin,cmdbPassword,cmdbUrl,cmdbObjectID,cmdbAttributes, expectedVendors):
	"""
	Compiles dict of switches with supplied filled 'Vendor' attribute (and it's found in expectedVendors list). 'IsDeployed' attribute is also checked, must be 'Deployed'.
	
	Функция составления словаря из коммутаторов, у которых указан атрибут Vendor и он есть в списке expectedVendors. Кроме того, проверяется атрибут IsDeployed, значение которого должно быть "В эксплуатации".
	cmdbAttributes - словарь из имён и ID атрибутов Vendor и IsDeployed.
	cmdbObjectID - ID объекта "коммутатор" в CMDB.
	expectedVendors - список из вендоров, коммутаторы которых надо проверять в CMDB.
	"""
	
	cmdbSession = cmdbConnector(cmdblogin,cmdbpassword,cmdburl)
	
	try:
		cmdbSession.initSession()	
	except RuntimeError as e:
		print(e)
	
	# .: Создаём список коммутаторов в формате {<имя коммутатора> : <местонахождение>}
	
	switches = cmdbSession.cmdbListObjects(cmdburl,cmdbObjectID,True,'101')
	switchesInAction = {}
	try:
		for switch in switches:			
			result = cmdbSession.cmdbGetObjectAttributeValues(cmdburl,switch,[cmdbAttributes['IsDeployed'],cmdbAttributes['Switch Role']],[],[cmdbAttributes['Vendor'],cmdbAttributes['Location']],[],False,'101')
			if result[cmdbAttributes['IsDeployed']].lower().encode('utf-8') == 'в эксплуатации' and result[cmdbAttributes['Vendor']][0] in expectedVendors:
				switchesInAction[switch] = {}
				switchesInAction[switch]['vendor'] = result[cmdbAttributes['Vendor']][0]
				switchesInAction[switch]['location'] = result[cmdbAttributes['Location']][0]
				switchesInAction[switch]['switch role'] = result[cmdbAttributes['Switch Role']].lower().encode('utf-8')
		return switchesInAction
	except IndexError as e:
		raise RuntimeError('IsDeployed or Location or Switch Role fields are empty')
		
def poolMultipleArgs(args):
	return collectMACAddressTable(*args)

def compileDictWithMacs(switchList,snmpCommunityName,procNum=4):
	"""
	Multiprocessing. Running no more thatn procNum functions at once. Getting MAC info.
	Запускаем одновременно не более procNum процессов получения SNMP-данных о MAC-адресах коммутаторов
	""" 
	
	swPoolArg = [(sw, snmpCommunityName, switchList[sw]) for sw in switchList.keys()]
	pool = Pool(procNum) 
	macDict = pool.map(poolMultipleArgs,swPoolArg)
        pool.close()
        pool.join()

	return macDict

def trimUplinkDownlinkPorts(macdict,snmpcommunity):
	"""
	Deleting ports, if 'Description' matches 'downlink' or 'uplink'.
	
	Удаляем из словаря с MAC-адресами порты, у которых ifAlias помечен как 'Downlink' или 'Uplink'
	"""
	
	strings = ['uplink','downlink']
	portDict = {}
	
	for sw in range(len(macdict)):
		if type(macdict[sw]) == dict and macdict[sw].has_key('SW'):
			result = findInterfacesByDescription(macdict[sw]['SW'],snmpcommunity,strings)
			if len(result) > 0:	
				portDict[macdict[sw]['SW']] = result

	tmpdict = macdict	
	
	
	for trimport in range(len(tmpdict)):

		if type(tmpdict[trimport]) == dict and tmpdict[trimport].has_key('SW') and tmpdict[trimport]['SW'] in portDict:
			try:
				for item in portDict[tmpdict[trimport]['SW']]:
					print(tmpdict[trimport]['SW'])
					del macdict[trimport][item]
			except (AttributeError, KeyError):
				continue

	return macdict
			

def pushMacsToDatabase(dbhost,dbname,dbusername,dbpassword,macs):
	"""
	Pushing result of compileDictWithMacs to database.
	
	Функция, заносящая данные по MAC-адресам коммутаторов в БД. Ожидает macs в виде, который отдаёт функция compileDictWithMacs
	"""
	
	mssqlSession = mssqlConnector(dbhost,dbname,dbusername,dbpassword)
	try:
		mssqlSession.initConnection()
	except RuntimeError as e:
		print(e)
		sys.exit(1)
		
	else:
		
		try:
			for sw in range(len(macs)):
				if type(macs[sw]) == dict and macs[sw].has_key('SW'):
					for port in macs[sw]:
						value = macs[sw][port] if port != 'SW' and port != 'Location' else None
						if value != None:
							for item in range(len(value)):
								query = ("INSERT INTO t_history ([Switch DNS Name],[Port Name],[MAC-address],[Timestamp],[VLAN],[Location]) VALUES (" + "'" + macs[sw]['SW'] + "'" + "," + "'" + str(port) + "'" + "," + "'" + str(value[item]['MAC']) + "'" + ",sysdatetime()," + "'" + str(value[item]['VLAN']) + "'" + "," + "'" + macs[sw]['Location'] + "'" + ")").encode('utf-8')
								mssqlSession.runQuery(query)
								
		except:
			print "Unexpected error:", sys.exc_info()[0]
			raise
		finally:
			mssqlSession.closeConn()
			
if __name__ == '__main__':
	
	try:
		switches = collectActiveSW(cmdblogin,cmdbpassword,cmdburl,cmdbObjectID,attribmapping,vendors)
		logging.info('Retrieved %s active switches from CMDB' % len(switches) )

		brSwitches = { switch : switches[switch]['location'] for switch in switches if switches[switch]['vendor'] == 'Brocade' and switches[switch]['switch role'] == 'доступ'}
		atSwitches = { switch : switches[switch]['location'] for switch in switches if switches[switch]['vendor'] == 'Allied Telesis' and switches[switch]['switch role'] == 'доступ'}

		if not brSwitches:
			logging.error('Empty list of Brocade switches!')
		if not atSwitches:
			logging.error('Empty list of Allied Telesyn switches!')
		if brSwitches and atSwitches:
			logging.info('Got %s Brocade switches and %s AT switches' % (len(brSwitches), len(atSwitches)) )
					
		brMacs = compileDictWithMacs(brSwitches,'brocadeKK')
		atMacs = compileDictWithMacs(atSwitches,'public')

		atTrimmedMacs = trimUplinkDownlinkPorts(atMacs,'public')
		brTrimmedMacs = trimUplinkDownlinkPorts(brMacs,'brocadeKK')
	
		# -- Объёдиняем списки MACов в один Merging MAC list of differenet switches to one -- #
		allMacs = atTrimmedMacs + brTrimmedMacs
	
		pushMacsToDatabase(dbhost,dbname,dbusername,dbpassword,allMacs)
	except ValueError as e:
		print ('Problem getting JSON from JIRA')