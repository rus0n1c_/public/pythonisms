Collects MAC-addresses found in MAC-address table for switch ports.

Workflow:

1. Connects to JIRA Insight CMDB.

2. Retrieves switch list with attribute 'Description' and value 'Deployed'

3. Compiles complete MAC-address table and VLAN for every port of every switch (OID 1.3.6.1.2.1.17.7.1.2.2.1.2)

4. Connects to MSSQL database and fills up corresponding rows and columns.

Cбор информации о MAC-адресах на портах коммутаторов. 

Алгоритм работы:

1. Скрипт подключается к Jira CMDB Insight

2. Получает оттуда список коммутаторов, у которых в поле Description стоит статус "В эксплуатации".

3. Для каждого коммутатора из списка по SNMP OID 1.3.6.1.2.1.17.7.1.2.2.1.2 получает таблицу с MAC-адресами на каждом из портов (кроме портов-аплинков), а также VLAN, в котором находится этот MAC.

4. Подключается к серверу MSSQL с базой данных, и заносит туда данные о всех MAC-адресах на каждом порту каждого коммутатора.

bin/main.py -- CMDB-interaction

macCollect/__init__.py -- MAC-collection.
