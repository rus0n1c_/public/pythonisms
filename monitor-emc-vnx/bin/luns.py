#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import os
import re

def parserFunction(argv=None):

	parser = argparse.ArgumentParser(description="Parse LUNs metric from file.")
	parser.add_argument('--file', help='File to parse',required=True)
	parser.add_argument('--lunname', help='LUN Name',required=True)
	parser.add_argument('--lunmetric', help='What to search for in file, see README for list of supported metrics',required=False)
	parser.add_argument('--onlytrim',choices=['0','1'],help='Get full data for selected LUN without specific metric',required=False)
	parsedParams = parser.parse_args(argv)
	if parsedParams.lunname and not parsedParams.lunmetric and not parsedParams.onlytrim:
		parser.error("--lunname requires --lunmetric to be set")
	elif parsedParams.lunmetric and not parsedParams.lunname:
		parser.error("--lunmetric requires --lunname to be set")	
	return parsedParams
	
if __name__ == "__main__":
	
	parsedValues = parserFunction()
	
	if not os.path.exists(parsedValues.file) or os.path.exists(parsedValues.file) and not os.path.isfile(parsedValues.file):
		print('Supplied file not found or is not a file. Exiting')
		exit(1)
		
	try:
		luns = open(parsedValues.file).read()
		if parsedValues.onlytrim and parsedValues.onlytrim == '1':
			regex = 'Name:\s+%s(?s).*?\n\n' % (parsedValues.lunname)
			metric = re.compile(regex)
			value = re.findall(metric,luns)
			print(value[0])
		else:
			regex = 'Name:\s+%s(.*\n){1,}?%s.*:\s+(.*)' % (parsedValues.lunname, parsedValues.lunmetric)
			metric = re.compile(regex)
			value = re.findall(metric, luns)[0][1]
			print(value)
	except (RuntimeError, IndexError) as e:
		print e