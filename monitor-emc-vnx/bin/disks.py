#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import os
import re

def parserFunction(argv=None):

	parser = argparse.ArgumentParser(description="Parse disks metric from file.")
	parser.add_argument('--file', help='File to parse',required=True)
	parser.add_argument('--disknumber', help='Disk to parse state of. Format X_Y_Z (Bus_Enclosure_Disk)',required=True)
	parser.add_argument('--diskmetric', help='What to search for in file',required=False)
	parser.add_argument('--onlytrim',choices=['0','1'],help='Get full data for selected disk without specific metric',required=False)
	parsedParams = parser.parse_args(argv)
	if parsedParams.disknumber and not parsedParams.diskmetric and not parsedParams.onlytrim:
		parser.error("--disknumber requires --diskmetric to be set")
	elif parsedParams.diskmetric and not parsedParams.disknumber:
		parser.error("--diskmetric requires --disknumber to be set")
	#elif parsedParams.onlytrim and parsedParams.onlytrim == '0' and 
	return parsedParams
	
if __name__ == "__main__":
	
	parsedValues = parserFunction()
	
	if not os.path.exists(parsedValues.file) or os.path.exists(parsedValues.file) and not os.path.isfile(parsedValues.file):
		print('Supplied file not found or is not a file. Exiting')
		exit(1)
	
	format = re.compile('\d{1,2}_\d{1,2}_\d{1,2}')
	if not re.match(format, parsedValues.disknumber):
		print('--disknumber is in wrong format. Exiting.')
		exit(1)
	
	try:
		disks = open(parsedValues.file).read()
		if parsedValues.onlytrim and parsedValues.onlytrim == '1':
			regex = 'Bus\s%s\sEnclosure\s%s\s+Disk\s%s(?s).*?\n\n' % (parsedValues.disknumber.split('_')[0], parsedValues.disknumber.split('_')[1], parsedValues.disknumber.split('_')[2])
			metric = re.compile(regex)
			value = re.findall(metric,disks)
			print(value[0])
		else:
			regex = 'Bus\s%s\sEnclosure\s%s\s+Disk\s%s\s(.*\n){1,}?%s:\s+(.*)' % (parsedValues.disknumber.split('_')[0], parsedValues.disknumber.split('_')[1], parsedValues.disknumber.split('_')[2], parsedValues.diskmetric)
			metric = re.compile(regex)
			value = re.findall(metric, disks)[0][1]
			print(value)
	except (RuntimeError, IndexError) as e:
		print e