#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import os
import re

def parserFunction(argv=None):

	parser = argparse.ArgumentParser(description="Parse SPs metric from file.")
	parser.add_argument('--file', help='File to parse',required=True)
	parser.add_argument('--spmetric', help='What to search for in file, see README for full list.',required=True)
	parsedParams = parser.parse_args(argv)
	return parsedParams
	
if __name__ == "__main__":
	
	parsedValues = parserFunction()
	
	if not os.path.exists(parsedValues.file) or os.path.exists(parsedValues.file) and not os.path.isfile(parsedValues.file):
		print('Supplied file not found or is not a file. Exiting')
		exit(1)
		
	try:
		spdata = open(parsedValues.file).read()
		regex = '%s.*:\s+(.*)' % parsedValues.spmetric
		metric = re.compile(regex)
		value = re.findall(metric, spdata)[0]
		print(value)
	except (RuntimeError, IndexError) as e:
		print e