#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import os
import re

def parserFunction(argv=None):

	parser = argparse.ArgumentParser(description="Parse Storage Pool metric from file.")
	parser.add_argument('--file', help='File to parse',required=True)
	parser.add_argument('--splname', help='Storage Pool name',required=True)
	parser.add_argument('--splmetric', help='What to search for in file, see README for full list.',required=False)
	parser.add_argument('--onlydisks',choices=['1','0'], help='If set instead of --splmetric and to 1, returns only disks of Storage Pool',required=False)
	parsedParams = parser.parse_args(argv)
	if not parsedParams.onlydisks and not parsedParams.splmetric:
		parser.error('Neither --splmetric not --onlydisks is set. Exiting')
	return parsedParams
	
if __name__ == "__main__":
	
	parsedValues = parserFunction()
	
	if not os.path.exists(parsedValues.file) or os.path.exists(parsedValues.file) and not os.path.isfile(parsedValues.file):
		print('Supplied file not found or is not a file. Exiting')
		exit(1)
		
	try:
		spdata = open(parsedValues.file).read()
		if parsedValues.onlydisks and parsedValues.onlydisks == '1':
			trim = 'Pool Name:  %s(?s).*?(?=LUNs)' % parsedValues.splname
			metric = re.compile(trim)
			tmp = re.findall(metric, spdata)[0]
			regex = 'Bus\s\d\sEnclosure\s\d\sDisk\s\d+'
			metric = re.compile(regex)
			value = re.findall(metric, tmp)
			# -- trimming duplicates --
			duplicates = set(value)
			duplicates = list(duplicates)
			print(duplicates)
		elif parsedValues.splmetric and not parsedValues.onlydisks:
			regex = 'Pool\s+Name:\s+%s(.*\n)+?%s:\s+(.*)' % (parsedValues.splname, parsedValues.splmetric)
			metric = re.compile(regex)
			value = re.findall(metric, spdata)#[0]
			print(value)
	except (RuntimeError, IndexError) as e:
		print e
		
		