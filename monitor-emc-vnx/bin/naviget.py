#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import argparse
import sys
import os
import json

def parserFunction(argv=None):

	parser = argparse.ArgumentParser(description="EMC VNX Monitoring using naviseccli tool")
	parser.add_argument('--username',help='VNX Username',required=True)
	parser.add_argument('--password',help='VNX Password', required=True)
	parser.add_argument('--scope',choices=['1','2'],help='Local or LDAP user?',required=True)
	parser.add_argument('--spaddress',help='VNX Storage Processor DNS-Name or IP-address',required=True)
	parser.add_argument('--navisecclipath',help='Naviseccli tool path', required=True)
	parser.add_argument('--command',help='Command to execute via navissecli. See README for full list of supported commands',required=True)
	parser.add_argument('--option',help='Optional flag for command. See README for full list of supported flags and commands',required=False)
	parser.add_argument('--tmpfolder',help='Temp folder to write temp files to',required=False)
	parser.add_argument('--savedump', choices=['0', '1'], help='File dump for supplied command is saved after completion. Name of the file is <command>.<option>.dump',required=False)
	
	parsedParams = parser.parse_args(argv)
		
	if parsedParams.option and not parsedParams.command:
		parser.error("--option requires --command to be set")
	return parsedParams
	
	
class vnxMonitoring():
	def __init__(self, username, password, scope, spAddress, navicliPath, command, option=None, tmpFolder=None, savedump=None):

		if not os.path.exists(navicliPath):
			raise RuntimeError('\nNavisecCLI binary not found at supplied path. Exiting.\n')
		else:
			self.username = username
			self.password = password
			self.scope = scope
			self.spAddress = spAddress
			self.navicliPath = navicliPath
			self.command = command
			self.tmpFolder = tmpFolder
			self.option = option
			self.savedump = savedump
			
	def vnxCheckAllowedCommands(self,command,option=None):
		"""
		Checking supplied commands is valid and can be processed by now
		"""
		self.command = command
		self.option = option
		
		allowedCommands = {
							'getall': None,
							'getdisk': ['discovery', 'discovery2', 'all'],
							'lun': ['discovery', 'list all'],
							'getcontrol': ['all'],
							'storagepool': ['list all'],
							'port': ['list reads writes bread bwrite qfull sfpstate','discovery']
							}
		
		if self.command not in allowedCommands:
			raise RuntimeError('Error. Supplied command not found in allowed commands list. Exiting.')
		elif self.command in allowedCommands and self.option and self.option not in allowedCommands[self.command]:
			raise RuntimeError('Error. Supplied command found, but option os wrong for this command.Exiting.')
		elif self.command in allowedCommands and self.option and self.option not in allowedCommands[self.command]:
			return True
										
	def vnxRunAllowedCommands(self,command,option=None):
		self.command = command
		self.option = option
		self.match = ''
		self.failureRegex = re.compile('Error returned from the Management Server')
		
		def vnxGetNaviseccliResponse():
			"""
			Small repetitive function to actually query SAN Storage via Naviseccli and try regex against the returned value
			"""
			self.tmpFileName = self.command + '.' + self.option + '.dump'
			self.tmpResultFile = os.path.join(self.tmpFolder, self.tmpFileName) if self.tmpFolder else os.path.join(self.tmpFileName)
			self.tmpResult = os.popen(self.navicliPath + " -User %s -Password %s -Scope %s -Address %s %s %s" % (self.username, self.password, self.scope, self.spAddress, self.command, self.option)).read()
			#self.query = os.system(self.navicliPath + " -User %s -Password %s -Scope %s -Address %s %s %s > %s" % (self.username, self.password, self.scope, self.spAddress, self.command, self.option, self.tmpResultFile))
			#self.tmpResult = open(self.tmpResultFile).read()
			# -- Checking savedump option -- #
			if self.savedump == '1':
				f = open(self.tmpResultFile,'w+')
				f.write(self.tmpResult)
				f.close()

			if not re.match(self.failureRegex, self.tmpResult):
				return(re.findall(self.match, self.tmpResult))
			else:
				raise RuntimeError(self.tmpResult)
				
		try:
			self.vnxCheckAllowedCommands(self.command,self.option)

			# -- Have to add dash manually due to argparse limitation with dashes. Quick, dirty and dumb, sorry. --
			
			if self.command == 'getcontrol' and self.option == 'all':
			# -- No regex here, returning all disk data -- #
				self.option = '-all'
				self.match = re.compile('^(?s).*$')
				self.value = vnxGetNaviseccliResponse()
				return self.value[0]
				
			if self.command == 'getdisk' and self.option == 'all':
			# -- No regex here, returning all disk data -- #
				self.option = '-all'
				self.match = re.compile('.*')
				self.value = vnxGetNaviseccliResponse()
				return self.value
				
			if self.command == 'getdisk' and self.option == 'discovery':
			# -- Forming Zabbix LLD JSON for Disks --
				self.option = '-all'
				self.match = re.compile('Bus\s(\d+)\sEnclosure\s*(\d+)\s+Disk\s(\d+)')
				self.zabbixJSON = []
				self.value = vnxGetNaviseccliResponse()
				for disk in self.value:
					self.zabbixJSONItems = {}
					self.zabbixJSONItems['{#DISK.BUS}'] = disk[0]
					self.zabbixJSONItems['{#DISK.ENCLOSURE}'] = disk[1]
					self.zabbixJSONItems['{#DISK.NUMBER}'] = disk[2]
					self.zabbixJSON.append(self.zabbixJSONItems)
					
				return json.dumps({'data':self.zabbixJSON},indent=4, separators=(',',':'))

			if self.command == 'getdisk' and self.option == 'discovery2':
			# -- Returning tuples of found Disks -- #
				self.option = '-all'
				self.match = re.compile('Bus\s(\d+)\sEnclosure\s*(\d+)\s+Disk\s(\d+)')
				self.value = vnxGetNaviseccliResponse()
				return self.value
				
			if self.command == 'lun' and self.option == 'list all':
			# -- No regex here, returning all LUN data -- #
				self.option = '-list -all'
				self.match = re.compile('.*')
				self.value = vnxGetNaviseccliResponse()
				return self.value
				
			if self.command == 'lun' and self.option == 'discovery':
						
			# -- Forming Zabbix LLD JSON for LUNs --
				self.option = '-list -all'
				self.match = re.compile('LOGICAL\sUNIT\sNUMBER(.*\n){1,}?Name:\s+(.*)')
				self.zabbixJSON = []
				self.value = vnxGetNaviseccliResponse()
				for lun in self.value:
					self.match = re.compile('Name:\s+%s(.*\n){1,}?Pool\sName:\s+(.*)' % lun[1])
					self.spool = re.findall(self.match, self.tmpResult)
					self.zabbixJSONItems = {}
					self.zabbixJSONItems['{#LUN.NAME}'] = lun[1]
					self.zabbixJSONItems['{#LUN.SPOOL}'] = self.spool[0][1]
					self.zabbixJSON.append(self.zabbixJSONItems)
				
				return json.dumps({'data':self.zabbixJSON},indent=4, separators=(',',':'))
				
			if self.command == 'disk' and self.option == 'list all':
			# -- No regex here, returning all disk data -- #
				self.option = '-list -all'
				self.match = re.compile('.*')
				self.value = vnxGetNaviseccliResponse()
				return self.value
				
			if self.command == 'storagepool' and self.option == 'list all':
			# -- No regex here, returning all disk data -- #
				self.option = '-list -all'
				self.match = re.compile('.*')
				self.value = vnxGetNaviseccliResponse()
				return self.value
				
			if self.command == 'port' and self.option == 'list reads writes bread bwrite qfull sfpstate':
				self.option = '-list -reads -writes -bread -bwrite -qfull -sfpstate'
				self.match = re.compile('^(?s).*$')
				self.value = vnxGetNaviseccliResponse()
				return self.value
			
			if self.command == 'port' and self.option == 'discovery':
				self.option = '-list -mac'
				self.match = re.compile('SP\sName:\s+SP\s(.*)\nSP\sPort\sID:\s+(\d)')
				self.value = vnxGetNaviseccliResponse()
				self.zabbixJSON = []
				self.value = vnxGetNaviseccliResponse()
				for port in self.value:
					self.zabbixJSONItems = {}
					self.zabbixJSONItems['{#PORT.SP}'] = port[0]
					self.zabbixJSONItems['{#PORT.ID}'] = port[1]
					self.zabbixJSON.append(self.zabbixJSONItems)
					
				return json.dumps({'data':self.zabbixJSON},indent=4, separators=(',',':'))
				
		except (RuntimeError, IndexError) as e:
			print(e)
			exit(1)
			
if __name__ == "__main__":
	
	
	parsedValues = parserFunction()
		
	try:
		monitor = vnxMonitoring(parsedValues.username,parsedValues.password,parsedValues.scope,parsedValues.spaddress,parsedValues.navisecclipath, parsedValues.command, parsedValues.option, parsedValues.tmpfolder, parsedValues.savedump)
		monitor.vnxCheckAllowedCommands(monitor.command, monitor.option)
		r = monitor.vnxRunAllowedCommands(monitor.command,monitor.option)
		print(r)
	except RuntimeError as e:
		print e
	
	