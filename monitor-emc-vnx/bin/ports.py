﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import os
import re

def parserFunction(argv=None):

	parser = argparse.ArgumentParser(description="Parse FC-ports metrics.")
	parser.add_argument('--file', help='File to parse',required=True)
	parser.add_argument('--port', help='FC-Port ID, format: "A_0, B_1',required=True)
	parser.add_argument('--portmetric', help='What to search for in file, see README for list of supported metrics',required=False)
	parser.add_argument('--onlytrim',choices=['0','1'],help='Get full data for selected FC-port',required=False)
	parsedParams = parser.parse_args(argv)
	if parsedParams.port and not parsedParams.portmetric and not parsedParams.onlytrim:
		parser.error("--lunname requires --lunmetric to be set")
	elif parsedParams.portmetric and not parsedParams.port:
		parser.error("--lunmetric requires --lunname to be set")	
	return parsedParams
	
if __name__ == "__main__":
	
	parsedValues = parserFunction()
	
	if not os.path.exists(parsedValues.file) or os.path.exists(parsedValues.file) and not os.path.isfile(parsedValues.file):
		print('Supplied file not found or is not a file. Exiting')
		exit(1)
		
	try:
		ports = open(parsedValues.file).read()
		
		if parsedValues.onlytrim and parsedValues.onlytrim == '1':
			regex = 'SP\sName:\s+SP\s%s\nSP\sPort\sID:\s+%s(?s).*?\n\n' % (parsedValues.port.split('_')[0], parsedValues.port.split('_')[1])
			metric = re.compile(regex)
			value = re.findall(metric,ports)
			print(value[0])
		else:
			regex = 'SP\sName:\s+SP\s%s\nSP\sPort\sID:\s+%s(.*\n)+?%s:\s+(.*)' % (parsedValues.port.split('_')[0], parsedValues.port.split('_')[1], parsedValues.portmetric)
			metric = re.compile(regex)
			value = re.findall(metric, ports)[0][1]
			print(value)
	except (RuntimeError, IndexError) as e:
		print e