﻿EMC VNX2 Monitoring solution using naviseccli tool.

Tested to work with  version 7.33.2.0.51 and OE Software version 05.33.009.5.155.

Your mileage may vary with other versions.

You also need Zabbix 3.4 at minimum to get correct calculated items (such as Service Time etc..)

bin/naviget.py - argparse-enabled script. Connects to Storage Processor and retrieves requested data. Saves data to temporary file <command>.<option>.dump and keeps or deletes file depending on --keepdump option (0 or 1).

bin/luns.py - parses file with results of 'naviseccli ... lun -list -all' command for supplied LUN Name and desired metric.

bin/disks.py - parses file with results of 'naviseccli ... getdisk -all' command for supplied disk name and desired metric.

bin/sp.py - parsed file with results of 'naviseccli ... getcontrol -all' command for supplied metric.

